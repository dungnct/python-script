import sys
import os
import shutil
import re
import codecs
from string import Template
serviceTemplateDir = sys.argv[1]
srcModuleName = sys.argv[2]
dstModuleName = sys.argv[3]
addModuleNames = sys.argv[3]
step = sys.argv[4]
def renameDir(rootDir, srcDir, dstDir):
  for path, subdirs, files in os.walk(rootDir):
    for name in subdirs:
      if(srcDir) in name:
        filePath = os.path.join(path,name)
        new_name = os.path.join(path,name.replace(srcDir, dstDir))
        os.rename(filePath, new_name)
def cloneDir(rootDir, srcDir, dstDir):
  # filePath = os.path.join(rootDir,srcDir)
  # new_name = os.path.join(rootDir,filePath.replace(srcDir, dstDir))
  # shutil.copytree(filePath, new_name)
  for path, subdirs, files in os.walk(rootDir):
    for name in subdirs:
      if(srcDir in name):
        filePath = os.path.join(path,name)
        new_name = name.replace(srcDir, dstDir)
        new_dir = os.path.join(path,new_name)
        if os.path.exists(new_dir) == False:
          shutil.copytree(filePath, new_dir)
        else:
          print('11111 ' + filePath)
          files = os.listdir(filePath)
          for file in files:
            srcFile = os.path.join(filePath,file)
            dstFile = os.path.join(new_dir,file)
            print('11111 ' + srcFile)
            print('11111 ' + dstFile)
            if os.path.isfile(srcFile):
              shutil.copy2(srcFile, new_dir)
            else:
              shutil.copytree(srcFile, dstFile)
def renameFile(rootDir, srcName, dstName):
  for path, subdirs, files in os.walk(rootDir):
    for name in files:
      if(srcName in name):
        filePath = os.path.join(path,name)
        new_name = os.path.join(path,name.replace(srcName, dstName))
        os.rename(filePath, new_name)
        
def cloneFile(srcFile, disFile):
  for path, subdirs, files in os.walk(serviceTemplateDir):
    for name in files:
      if(srcFile in name):
        filePath = os.path.join(path,name)
        new_name = os.path.join(path,name.replace(srcFile, disFile))
        shutil.copytree(filePath, new_name)
        
def addStringModule(rootDir, addModuleName):
  addModuleNameCapitalize = addModuleName.capitalize()
  addModuleNameModuleUpper = addModuleName.upper()
  lines = []
  #input file
  filePath = rootDir + "\\src\\app.module.ts"
  fin = codecs.open(filePath, "r", 'UTF-8')
  for line in fin:
    lines.append(line)
    if(line.find("import { StsClient } from \"./modules/mgs-sender/sts.client\";") != -1):
      lines.append("import { " + addModuleNameCapitalize + "DomainModule } from \"./modules/" + addModuleName + ".domain/module\";\n")
      lines.append("import { " + addModuleNameCapitalize + "QuerySideModule } from \"./modules/" + addModuleName + ".queryside/module\";\n")
    if(line.find("const routes: Routes = [") != -1):
      strTemplate = "{path: \"/domain\",module: SampleDomainModule,},{path: \"/query\",module: SampleQuerySideModule,},\n"
      addString = strTemplate.replace("Sample", addModuleNameCapitalize)
      lines.append(addString)
    if(line.find("ListenerModule,") != -1):
      lines.append(addModuleNameCapitalize + "DomainModule,\n")
      lines.append(addModuleNameCapitalize + "QuerySideModule,\n")
  # close input files
  fin.close()
  #output file to write the result to
  os.remove(filePath)
  fout = codecs.open(filePath, "w", 'UTF-8')
  fout.writelines(lines)
  # close output files
  fout.close()
  
  lines = []
  #input file
  filePath = rootDir + "\\src\\modules\\shared\\enum\\permission.enum.ts"
  fin = codecs.open(filePath, "r", 'UTF-8')
  for line in fin:
    lines.append(line)
    if(line.find("export enum PermissionEnum {") != -1):
      strTemplate = "//sample\nSAMPLE_CREATE = \"sample.create\",\nSAMPLE_UPDATE = \"sample.update\",\nSAMPLE_DELETE = \"sample.delete\",\nSAMPLE_GET_ALL = \"sample.get.all\",SAMPLE_GET_ID = \"sample.get.id\",\n"
      addString = strTemplate.replace("sample", addModuleName)
      addString = addString.replace("Sample", addModuleNameCapitalize)
      addString = addString.replace("SAMPLE", addModuleNameModuleUpper)
      lines.append(addString)
      
  # close input files
  fin.close()
  #output file to write the result to
  os.remove(filePath)
  fout = codecs.open(filePath, "w", 'UTF-8')
  fout.writelines(lines)
  # close output files
  fout.close()
  
  lines = []
  #input file
  filePath = rootDir + "\\src\\modules\\shared\\constant\\common.const.ts"
  fin = codecs.open(filePath, "r", 'UTF-8')
  for line in fin:
    lines.append(line)
    if(line.find("static CODE_COLLECTION = \"code-generates\";") != -1):
      strTemplate = "static SAMPLE_DOMAIN_MODEL_TOKEN = \"Sample-Domain-ModelToken\";static SAMPLE_QUERY_MODEL_TOKEN = \"Sample-Query-ModelToken\";static SAMPLE_EVENTS = \"events-sample\";static SAMPLE_AGGREGATE_NAME = \"sample\";static SAMPLE_COLLECTION = \"samples\";\n"
      addString = strTemplate.replace("sample", addModuleName)
      addString = addString.replace("Sample", addModuleNameCapitalize)
      addString = addString.replace("SAMPLE", addModuleNameModuleUpper)
      lines.append(addString)
    if(line.find("  static AGGREGATES = {") != -1):
      strTemplate = "SAMPLE: {NAME: CommonConst.SAMPLE_AGGREGATE_NAME,CREATED: CommonConst.SAMPLE_AGGREGATE_NAME + \"Created\",UPDATED: CommonConst.SAMPLE_AGGREGATE_NAME + \"Updated\",DELETED: CommonConst.SAMPLE_AGGREGATE_NAME + \"Deleted\",EVENTS: \"events-\" + CommonConst.SAMPLE_AGGREGATE_NAME,COLLECTION: CommonConst.SAMPLE_AGGREGATE_NAME,},\n"
      addString = strTemplate.replace("sample", addModuleName)
      addString = addString.replace("Sample", addModuleNameCapitalize)
      addString = addString.replace("SAMPLE", addModuleNameModuleUpper)
      lines.append(addString)
      
      
  # close input files
  fin.close()
  #output file to write the result to
  os.remove(filePath)
  fout = codecs.open(filePath, "w", 'UTF-8')
  fout.writelines(lines)
  # close output files
  fout.close()

def replaceContent(rootDir, oldString, newString):
  for path, subdirs, files in os.walk(rootDir):
    for name in files:
      if(name.lower().endswith(('.ts', '.json')) or name.lower().startswith(('.env'))):
        filePath = os.path.join(path,name)
        print(filePath)
        #input file
        fin = codecs.open(filePath, "r", 'UTF-8')
        #output file to write the result to
        oldPath = filePath
        newPath = filePath + ".bak"
        fout = codecs.open(newPath, "w", 'UTF-8')
        #for each line in the input file
        for line in fin:
          # read replace the string and write to output file
          newLine = line.replace(oldString, newString)
          fout.write(newLine)
        # close input and output files
        fin.close()
        fout.close()
        os.remove(oldPath)
        os.rename(newPath, oldPath)
      
def main():
  # clone module
  if(step == 'clone'):
    print('clone')
    moduleNames = addModuleNames.split(',')
    for addModuleName in moduleNames:
      print(addModuleName)
      cloneDir(serviceTemplateDir + '\\src\\', srcModuleName, addModuleName)
      renameFile(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.domain',srcModuleName, addModuleName)
      renameFile(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.queryside',srcModuleName, addModuleName)
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.domain',srcModuleName, addModuleName)
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.queryside',srcModuleName, addModuleName)
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.domain',srcModuleName.capitalize(), addModuleName.capitalize())
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.queryside',srcModuleName.capitalize(), addModuleName.capitalize())
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.domain',srcModuleName.upper(), addModuleName.upper())
      replaceContent(serviceTemplateDir + '\\src\\modules\\' + addModuleName + '.queryside',srcModuleName.upper(), addModuleName.upper())
      replaceContent(serviceTemplateDir + '\\src\\modules\\shared\\services\\' + addModuleName + '\\', srcModuleName.capitalize(), addModuleName.capitalize())
      addStringModule(serviceTemplateDir, addModuleName)

  # replace module
  if(step == 'replace'):
    renameDir(serviceTemplateDir, srcModuleName, dstModuleName)
    renameFile(serviceTemplateDir,srcModuleName, dstModuleName)
    replaceContent(serviceTemplateDir, srcModuleName, dstModuleName)
    replaceContent(serviceTemplateDir, srcModuleName.capitalize(), dstModuleName.capitalize())
    replaceContent(serviceTemplateDir, srcModuleName.upper(), dstModuleName.upper())
    addStringModule(serviceTemplateDir, dstModuleName)
main()