import { Module } from "@nestjs/common";
import { ListenerController } from "./listener.controller";
import { SampleQuerySideModule } from "../sample.queryside/module";
// import { EmployeeQuerySideModule } from "../employee.query/module";

@Module({
  imports: [
    SampleQuerySideModule,
    // EmployeeQuerySideModule
  ],
  controllers: [ListenerController],
})
export class ListenerModule {}
