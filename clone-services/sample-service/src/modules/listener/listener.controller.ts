import { Controller } from "@nestjs/common";
import { MessagePattern } from "@nestjs/microservices";
import { CmdPatternConst, CommonConst } from "../shared/constant";
// import { EmployeeService } from "../employee.query/service";

@Controller("listener")
export class ListenerController {
  private readonly context = ListenerController.name;

  constructor() {} // private readonly employeeService: EmployeeService

  // @MessagePattern({ cmd: CmdPatternConst.LISTENER.EMPLOYEE })
  // listenerEmployee(pattern: any) {
  //   const data = pattern.data;
  //   const action = data.action;
  //   const model = data.model;
  //   switch (action) {
  //     case CommonConst.AGGREGATES_LISTENER.EMPLOYEE.CREATED:
  //       this.employeeService.create(model);
  //       break;
  //     case CommonConst.AGGREGATES_LISTENER.EMPLOYEE.UPDATED:
  //       this.employeeService.update(model);
  //       break;
  //     case CommonConst.AGGREGATES_LISTENER.EMPLOYEE.DELETED:
  //       this.employeeService.delete(model);
  //       break;
  //     case CommonConst.AGGREGATES_LISTENER.EMPLOYEE.LIST_UPDATED:
  //       this.employeeService.updateList(model);
  //       break;
  //     default:
  //       break;
  //   }
  // }
}
