import * as mongoose from "mongoose";

export const ProjectSchema = new mongoose.Schema(
  {
    id: { type: String },
    name: { type: String },
    code: { type: String },
  },
  { _id: false }
);

export const TransactionSchema = new mongoose.Schema(
  {
    id: { type: String },
    bookingTicketCode: { type: String },
    escrowTicketCode: { type: String },
    customer: { type: Object },
    customer2: { type: Object },
    project: { type: ProjectSchema },
    propertyUnit: { type: Object },
  },
  { _id: false }
);
