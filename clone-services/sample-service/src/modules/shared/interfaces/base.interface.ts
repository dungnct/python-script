export interface IBaseInterface {
  description: string;
  active: boolean;
  softDelete: boolean;
  modifiedBy: string;
}
