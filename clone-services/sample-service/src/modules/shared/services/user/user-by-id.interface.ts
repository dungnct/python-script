export interface IUserResquest {
  id: string;
}

export interface IUserResponse {
  id: string;
  name: string;
  email: string;
  roles: object;
  status: string;
}
