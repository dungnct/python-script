export interface IAclResponse {
  role: string;
  resource: string;
  action: string;
}

export interface IAclByIdRequest {
  featureId: string;
}
