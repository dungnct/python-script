import { IBaseInterface } from "../../../interfaces/base.interface";

export interface ISample extends IBaseInterface {
  id: string;
  description: string;
}
