export class CommonConst {
  static DATABASE_SERVICE_READ_MODEL = "msx-sample-readmodel";
  static DATABASE_SERVICE_EVENTS_MODEL = "msx-sample-eventstore";
  static HEADER_PARTERN_STR = "pattern";

  static DOMAIN_CONNECTION_TOKEN = "Domain-DbConnectionToken";
  static QUERY_CONNECTION_TOKEN = "Query-DbConnectionToken";
  static DOMAIN_MODEL_TOKEN = "Domain-ModelToken";
  static QUERY_MODEL_TOKEN = "Query-ModelToken";
  static CODE_COLLECTION = "code-generates";
	
	


  static AGGREGATES_LISTENER = {
    EMPLOYEE: {
      NAME: "employee",
      CREATED: "employeeCreated",
      UPDATED: "employeeUpdated",
      LIST_UPDATED: "employeeListUpdated",
      DELETED: "employeeDeleted",
    },
  };
  static AGGREGATE_NAMES(): Object[] {
    return Object.keys(this.AGGREGATES).map((key) => this.AGGREGATES[key].NAME);
  }

  static AGGREGATES = {
  };
}
