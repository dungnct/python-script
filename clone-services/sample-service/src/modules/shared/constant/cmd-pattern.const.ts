export class CmdPatternConst {
  static USER_EXISTENCE_CMD = "msx-adsg.user.existence";

  static SETUP_FEATURE = "msx-adsg.feature.setup";
  static VERIFY_USER = "msx-adsm.sts.findUserByMsx";

  static SEND_REGISTER_AUTO_USER = "msx-adsg-mailer-pwdUser";
  static SEND_LOG = "msx-adsg.logger.log";
  static SEND_ERROR = "msx-adsg.logger.error";

  static CMD_PATTERN = "msx-adsg";
  static NOTIFY = {
    INFO: CmdPatternConst.CMD_PATTERN + ".notification.info",
    ERROR: CmdPatternConst.CMD_PATTERN + ".notification.error",
  };

  static USER = {
    SIGNED: CmdPatternConst.CMD_PATTERN + ".user.signed",
    RESET_ROLE: CmdPatternConst.CMD_PATTERN + ".user.role.reset",
    CREATE_USER_WITH_PASSWORD:
      CmdPatternConst.CMD_PATTERN + ".user.create.with.password",
    GET_ROLE: CmdPatternConst.CMD_PATTERN + ".user.get.role",
  };

  static NOTIFICATION = {
    MESSAGE: CmdPatternConst.CMD_PATTERN + ".notification.message",
  };
  static LISTENER = {
    EMPLOYEE: CmdPatternConst.CMD_PATTERN + ".employee.listener",
  };
}
