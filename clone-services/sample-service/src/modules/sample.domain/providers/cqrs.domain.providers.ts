import { Connection } from "mongoose";
import { CqrsDomainSchema } from "../schemas/cqrs.domain.schema";
import { CommonConst } from "../../shared/constant";

export const CqrsProviders = [
  {
    provide: CommonConst.SAMPLE_DOMAIN_MODEL_TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(CommonConst.SAMPLE_EVENTS, CqrsDomainSchema),
    inject: [CommonConst.DOMAIN_CONNECTION_TOKEN],
  },
];
