import { Model } from "mongoose";
import { Inject, Injectable } from "@nestjs/common";
import { IEventStreamDocument } from "../../shared/eventStream/interfaces/event-stream-document.interface";
import { BaseEventStream } from "../../shared/eventStream/models/base-event-stream.model";
import { SampleCreatedEvent } from "../events/event-stream-created.evt";
import { CommonConst } from "../../shared/constant/index";

@Injectable()
export class SampleEventRepository {
  constructor(
    @Inject(CommonConst.SAMPLE_DOMAIN_MODEL_TOKEN)
    private readonly repository: Model<IEventStreamDocument>
  ) {}

  async createEvent(eventModel: SampleCreatedEvent): Promise<any> {
    const baseEventStream = eventModel.baseEventStream;
    const payload = eventModel.commandModel;
    const msg = eventModel.msg;

    baseEventStream.payload = payload;
    baseEventStream.returnMessagePattern = msg;

    return this.findEventStreamById(baseEventStream.streamId).then(
      (response) => {
        if (response) baseEventStream.streamId = response.streamId;
        return this.createEventStream(baseEventStream);
      }
    );
  }

  async findEventStreamById(streamId: string) {
    return this.repository
      .findOne({ streamId })
      .sort({ commitStamp: -1 })
      .limit(1);
  }

  private async createEventStream(baseEventStream: BaseEventStream) {
    return this.repository.create(baseEventStream);
  }
}
