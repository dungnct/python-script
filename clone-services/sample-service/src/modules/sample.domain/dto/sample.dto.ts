import { IsNotEmpty, IsNumber, IsString, IsEnum } from "class-validator";
import { ClassBased } from "../../shared/classes/class-based";
import { ISample } from "../../shared/services/sample/interfaces/interface";

export class SampleDto extends ClassBased implements ISample {
  id: string;
  description: string;
}
export class CreateSampleDto extends SampleDto {
  @IsString()
  @IsNotEmpty()
  name: string;
}
export class UpdateSampleDto extends SampleDto {
  @IsString()
  @IsNotEmpty()
  id: string;

  name: string;
}
