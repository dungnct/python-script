import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { IResult } from "../../../shared/interfaces/result.interface";
import { isNullOrUndefined } from "util";
import { BaseEventStream } from "../../../shared/eventStream/models/base-event-stream.model";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";
import { UpdateSampleCommand } from "../impl/update-sample.cmd";
import { SampleEventRepository } from "../../repository/sample.event.repository";
import { SampleQueryRepository } from "../../../sample.queryside/repository/sample.query.repository";
import { SampleDomainAggregateModel } from "../../models/sample.domain-aggregate.model";

const clc = require("cli-color");

@CommandHandler(UpdateSampleCommand)
export class UpdateSampleCommandHandler
  implements ICommandHandler<UpdateSampleCommand> {
  private readonly context = UpdateSampleCommandHandler.name;
  /**
   * constant variable will set in configuration file,
   * will refactor in next version
   */
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  private readonly eventName: string = CommonConst.AGGREGATES.SAMPLE.UPDATED;

  /**
   * will refactor in next version for IResult pattern
   */
  private readonly result: IResult = {
    msg: "Update Command Handler Excecuted.",
    err: null,
    data: null,
  };

  constructor(
    private readonly repository: SampleEventRepository,
    private readonly publisher: EventPublisher,
    private readonly queryRepository: SampleQueryRepository,
    private readonly loggerService: MsxLoggerService
  ) {}

  // execute Cteate Sample Command
  async execute(command: UpdateSampleCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Update ${this.aggregateName} cmd ...`)
    );
    const { messagePattern, id, commandModel } = command;
    const es = await this.repository.findEventStreamById(commandModel.id);

    /**
     * this code block only works for payload relationship with other entities
     */
    if (es && es.payload) {
      const payload: any = es.payload;
      // if existed object and have no new one
      // keep existing object
      if (payload) {
        commandModel.id = es.streamId;
        // commandModel.name = (commandModel.name || payload.name);
        commandModel.description =
          commandModel.description || payload.description;
        commandModel.active = commandModel.active || payload.active;
        commandModel.softDelete = commandModel.softDelete || payload.softDelete;
        commandModel.modifiedBy = commandModel.modifiedBy || payload.modifiedBy;
      }
    }

    const eventStream = new BaseEventStream();
    eventStream.id = id;
    eventStream.aggregateId = id;

    if (isNullOrUndefined(commandModel.id)) commandModel.id = id;
    eventStream.streamId = commandModel.id;

    eventStream.aggregate = this.aggregateName;
    eventStream.eventName = this.eventName;

    commandModel.eventName = this.eventName;
    commandModel.actionName = messagePattern;

    // aggregate model
    const aggregateModel = new SampleDomainAggregateModel(eventStream);
    const cmdPublisher = this.publisher.mergeObjectContext(aggregateModel);
    // const msg = `${this.aggregateName}.${messagePattern}`;
    const msg = `${this.aggregateName}`;
    cmdPublisher.addItem(msg, commandModel);
    cmdPublisher.commit();

    this.result.msg = "Update Command Handler Excecuted.";
  }
}
