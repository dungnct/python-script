import { CreateSampleCommandHandler } from "./create-sample.cmd.handler";
import { UpdateSampleCommandHandler } from "./update-sample.cmd.handler";
import { DeleteSampleCommandHandler } from "./delete-sample.cmd.handler";

export const SampleDomainCommandHandlers = [
  CreateSampleCommandHandler,
  UpdateSampleCommandHandler,
  DeleteSampleCommandHandler,
];
