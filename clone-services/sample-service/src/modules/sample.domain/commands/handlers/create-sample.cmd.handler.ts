import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { IResult } from "../../../shared/interfaces/result.interface";

/**
 * repository from query side
 */
import { BaseEventStream } from "../../../shared/eventStream/models/base-event-stream.model";
import { isNullOrUndefined } from "util";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";
import { CreateSampleCommand } from "../impl/create-sample.cmd";
import { SampleEventRepository } from "../../repository/sample.event.repository";
import { SampleQueryRepository } from "../../../sample.queryside/repository/sample.query.repository";
import { SampleDomainAggregateModel } from "../../models/sample.domain-aggregate.model";

const clc = require("cli-color");

@CommandHandler(CreateSampleCommand)
export class CreateSampleCommandHandler
  implements ICommandHandler<CreateSampleCommand> {
  private readonly context = CreateSampleCommandHandler.name;
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  private readonly eventName: string = CommonConst.AGGREGATES.SAMPLE.CREATED;
  private readonly result: IResult = {
    msg: "Create Command Handler Excecuted.",
    err: null,
    data: null,
  };

  constructor(
    private readonly repository: SampleEventRepository,
    private readonly publisher: EventPublisher,
    private readonly queryRepository: SampleQueryRepository,
    private readonly loggerService: MsxLoggerService
  ) {}

  // execute Cteate Sample Command
  async execute(command: CreateSampleCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Create ${this.aggregateName} cmd ...`)
    );

    const { messagePattern, id, commandModel } = command;
    const eventStream = new BaseEventStream();
    eventStream.id = id;
    eventStream.aggregateId = id;

    if (!commandModel.id) {
      commandModel.id = id;
    }
    eventStream.streamId = commandModel.id;

    eventStream.aggregate = this.aggregateName;
    eventStream.eventName = this.eventName;

    commandModel.eventName = this.eventName;
    commandModel.actionName = messagePattern;

    const aggregateModel = new SampleDomainAggregateModel(eventStream);

    // pulisher
    const cmdPublisher = this.publisher.mergeObjectContext(aggregateModel);
    // const msg = `${this.aggregateName}.${messagePattern}`;
    const msg = `${this.aggregateName}`;
    cmdPublisher.addItem(msg, commandModel);
    cmdPublisher.commit();

    this.result.msg = "Create Command Handler Excecuted.";
  }
}
