import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { Client, ClientProxy, Transport } from "@nestjs/microservices";
import { IResult } from "../../../shared/interfaces/result.interface";
import { InfoMessagePattern } from "../../../shared/interfaces/messaging-pattern.interface";
import { isNullOrUndefined } from "util";
import { BaseEventStream } from "../../../shared/eventStream/models/base-event-stream.model";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";
import { DeleteSampleCommand } from "../impl/delete-sample.cmd";
import { SampleEventRepository } from "../../repository/sample.event.repository";
import { SampleDomainAggregateModel } from "../../models/sample.domain-aggregate.model";

const clc = require("cli-color");

@CommandHandler(DeleteSampleCommand)
export class DeleteSampleCommandHandler
  implements ICommandHandler<DeleteSampleCommand> {
  private readonly context = DeleteSampleCommandHandler.name;
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  private readonly eventName: string = CommonConst.AGGREGATES.SAMPLE.DELETED;
  private readonly result: IResult = {
    msg: "Delete Command Handler Excecuted.",
    err: null,
    data: null,
  };

  @Client({ transport: Transport.REDIS })
  client: ClientProxy;
  constructor(
    private readonly repository: SampleEventRepository,
    private readonly publisher: EventPublisher,
    private readonly loggerService: MsxLoggerService
  ) {}

  // execute Cteate Sample Command
  async execute(command: DeleteSampleCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Delete ${this.aggregateName} cmd ...`)
    );
    const { messagePattern, id, commandModel } = command;
    const es = await this.repository.findEventStreamById(commandModel.id);

    if (isNullOrUndefined(es)) {
      this.result.msg = "No data to delele";
      this.result.err = new Error("Null or Undefined");

      const pattern = { cmd: "msx-adsg.notification.info" };
      const messagingPattern = new InfoMessagePattern(
        messagePattern,
        this.result
      );
      this.client.send<any>(pattern, messagingPattern).subscribe();
    }

    /**
     * this code block only works for payload relationship with other entities
     */
    if (es && es.payload) {
      const payload: any = es.payload;
      // if existed object and have no new one
      // keep existing object
      if (payload) {
        commandModel.id = es.streamId;
        // commandModel.name = (commandModel.name || payload.name);
        commandModel.description =
          commandModel.description || payload.description;
        commandModel.active = commandModel.active || payload.active;
        commandModel.softDelete = commandModel.softDelete || payload.softDelete;
        commandModel.modifiedBy = commandModel.modifiedBy || payload.modifiedBy;
      }
    }

    const eventStream = new BaseEventStream();
    eventStream.id = id;
    eventStream.aggregateId = id;

    if (isNullOrUndefined(commandModel.id)) commandModel.id = id;
    eventStream.streamId = commandModel.id;

    // if (isNullOrUndefined(es.streamId)) eventStream.streamId = commandModel.id;
    // else eventStream.streamId = es.streamId;

    eventStream.aggregate = this.aggregateName;
    eventStream.eventName = this.eventName;

    commandModel.eventName = this.eventName;
    commandModel.actionName = messagePattern;

    // aggregate model
    const aggregateModel = new SampleDomainAggregateModel(eventStream);
    const cmdPublisher = this.publisher.mergeObjectContext(aggregateModel);
    // const msg = `${this.aggregateName}.${messagePattern}`;
    const msg = `${this.aggregateName}`;
    cmdPublisher.addItem(msg, commandModel);
    cmdPublisher.commit();

    this.result.msg = "Delete Command Handler Excecuted.";
  }
}
