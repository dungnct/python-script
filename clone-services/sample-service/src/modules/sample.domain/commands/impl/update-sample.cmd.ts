import { ICommand } from "@nestjs/cqrs";
import { CommandModel } from "../../../shared/eventStream/models/command.model";

/**
 * id: commandId
 * commandModel: CommandModel object
 */
export class UpdateSampleCommand implements ICommand {
  constructor(
    public readonly messagePattern: string,
    public readonly id: string,
    public readonly commandModel: CommandModel
  ) {}
}
