import {
  Controller,
  Post,
  Put,
  Body,
  Headers,
  UseGuards,
  UseInterceptors,
  Param,
  Delete,
} from "@nestjs/common";
import { LoggingInterceptor } from "../../common/interceptors/logging.interceptor";
import { SampleDomainService } from "./service";
import { Action } from "../shared/enum/action.enum";
import { RolesGuard } from "../../common/guards/roles.guard";
import { CreateSampleDto, UpdateSampleDto } from "./dto/sample.dto";
import { AuthGuard } from "@nestjs/passport";
import { ACGuard, UseRoles } from "nest-access-control";
import { Usr } from "../shared/services/user/decorator/user.decorator";
import { PermissionEnum } from "../shared/enum/permission.enum";
import { ValidationPipe } from "../../common/pipes/validation.pipe";
@Controller("v1/sample")
@UseInterceptors(LoggingInterceptor)
@UseGuards(AuthGuard("jwt"))
export class SampleDomainController {
  private actionName: string = Action.NOTIFY;
  constructor(private readonly sampleService: SampleDomainService) {}

  @UseGuards(RolesGuard, ACGuard)
  @UseRoles({
    resource: PermissionEnum.SAMPLE_CREATE, // => feature name
    action: "read",
    possession: "own",
  })
  @Post()
  async createSample(
    @Usr() user,
    @Body(new ValidationPipe()) dto: CreateSampleDto,
    @Headers("act") actionName?: string
  ) {
    this.actionName = actionName || this.actionName;
    return await this.sampleService.createSample(user, dto, this.actionName);
  }

  @UseGuards(RolesGuard, ACGuard)
  @UseRoles({
    resource: PermissionEnum.SAMPLE_UPDATE, // => feature name
    action: "read",
    possession: "own",
  })
  @Put()
  async updateSample(
    @Usr() user,
    @Body(new ValidationPipe()) dto: UpdateSampleDto,
    @Headers("act") actionName?: string
  ) {
    this.actionName = actionName || this.actionName;
    return await this.sampleService.updateSample(user, dto, this.actionName);
  }

  @UseGuards(RolesGuard, ACGuard)
  @UseRoles({
    resource: PermissionEnum.SAMPLE_DELETE, // => feature name
    action: "read",
    possession: "own",
  })
  @Delete(":id")
  async deleteSample(
    @Usr() user,
    @Param("id") id: string,
    @Headers("act") actionName?: string
  ) {
    this.actionName = actionName || this.actionName;
    return await this.sampleService.deleteSample(user, id, this.actionName);
  }
}
