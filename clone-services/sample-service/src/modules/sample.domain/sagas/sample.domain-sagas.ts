import { Injectable } from "@nestjs/common";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ICommand, ofType, Saga } from "@nestjs/cqrs";
import { SampleCreatedEvent } from "../events/event-stream-created.evt";
import { CreateSampleQueryCommand } from "../../sample.queryside/commands/impl/create-sample.query.cmd";
import { UpdateSampleQueryCommand } from "../../sample.queryside/commands/impl/update-sample.query.cmd";
import { DeleteSampleQueryCommand } from "../../sample.queryside/commands/impl/delete-sample.query.cmd";
import { CommonConst, CmdPatternConst } from "../../shared/constant/index";
import { MsxLoggerService } from "../../logger/logger.service";

const clc = require("cli-color");

@Injectable()
export class SampleDomainSagas {
  private readonly context = SampleDomainSagas.name;
  constructor(private readonly loggerService: MsxLoggerService) {}

  @Saga()
  querySide = (events$: Observable<any>): Observable<ICommand> => {
    return events$.pipe(
      ofType(SampleCreatedEvent),
      map((event: any) => {
        console.log(clc.redBright("Inside [SampleSagas] Saga"));
        const baseEventStream = event.baseEventStream;
        this.loggerService.log(
          this.context,
          "sample domain | sagas | baseEventStream.streamId ==>",
          baseEventStream.streamId
        );

        let cmd: ICommand;
        switch (baseEventStream.payload.eventName) {
          case CommonConst.AGGREGATES.SAMPLE.CREATED:
            cmd = new CreateSampleQueryCommand(
              baseEventStream.messagePattern,
              baseEventStream.streamId,
              baseEventStream.payload
            );
            break;
          case CommonConst.AGGREGATES.SAMPLE.UPDATED:
            cmd = new UpdateSampleQueryCommand(
              baseEventStream.messagePattern,
              baseEventStream.streamId,
              baseEventStream.payload
            );
            break;
          case CommonConst.AGGREGATES.SAMPLE.DELETED:
            cmd = new DeleteSampleQueryCommand(
              baseEventStream.messagePattern,
              baseEventStream.streamId,
              baseEventStream.payload
            );
            break;
        }
        return cmd;
      })
    );
  };
}
