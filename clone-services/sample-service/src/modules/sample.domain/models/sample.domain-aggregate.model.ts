import { AggregateRoot } from "@nestjs/cqrs";
import { SampleCreatedEvent } from "../events/event-stream-created.evt";
import { BaseEventStream } from "../../shared/eventStream/models/base-event-stream.model";
import { CommandModel } from "../../shared/eventStream/models/command.model";

export class SampleDomainAggregateModel extends AggregateRoot {
  constructor(private readonly baseEventStream: BaseEventStream) {
    super();
  }

  addItem(messagePattern: string, commandModel?: CommandModel) {
    this.apply(
      new SampleCreatedEvent(this.baseEventStream, commandModel, messagePattern)
    );
  }
}
