import { CqrsModule } from "@nestjs/cqrs";
import { Module } from "@nestjs/common";
import { SampleDomainCommandHandlers } from "./commands/handlers";
import { SampleDomainEventHandlers } from "./events";
import { SampleDomainSagas } from "./sagas/sample.domain-sagas";
import { SampleDomainController } from "./controller";
import { SampleDomainService } from "./service";
import { SampleEventRepository } from "./repository/sample.event.repository";
import { DomainDatabaseModule } from "../database/domain/domain.database.module";
import { CqrsProviders } from "./providers/cqrs.domain.providers";
import { SampleQuerySideModule } from "../sample.queryside/module";
import { MgsSenderModule } from "../mgs-sender/mgs-sender.module";
import { LoggerModule } from "../logger/logger.module";
import { CodeGenerateModule } from "../code-generate/module";

@Module({
  imports: [
    CqrsModule,
    DomainDatabaseModule,
    SampleQuerySideModule,
    MgsSenderModule,
    CodeGenerateModule,
    LoggerModule,
  ],
  controllers: [SampleDomainController],
  providers: [
    SampleDomainService,
    SampleEventRepository,
    SampleDomainSagas,
    ...CqrsProviders,
    ...SampleDomainCommandHandlers,
    ...SampleDomainEventHandlers,
  ],
})
export class SampleDomainModule {}
