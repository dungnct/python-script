import * as mongoose from "mongoose";
const uuid = require("uuid");
const beautifyUnique = require("mongoose-beautiful-unique-validation");

export const payloadSchema = new mongoose.Schema({
  _id: { type: String },
  id: { type: String, default: uuid.v4 },
  code: { type: String },
  name: { type: String },

  description: { type: String, default: "" },
  modifiedBy: String,
  createdDate: { type: Date, default: () => Date.now() },
  modifiedDate: { type: Date, default: () => Date.now() },
  eventName: { type: String },
  actionName: { type: String },
  revision: { type: Number, index: true },
});

payloadSchema.pre("save", function (next) {
  this._id = this.get("id");
  next();
});

// Enable beautifying on this schema
payloadSchema.plugin(beautifyUnique);
// ============

export const CqrsDomainSchema = new mongoose.Schema({
  _id: { type: String },
  id: { type: String, index: true },
  streamId: { type: String, index: true },
  aggregate: String,
  aggregateId: String,
  context: String,
  streamRevision: Number,
  commitId: String,
  commitSequence: Number,
  commitStamp: { type: Date, default: () => Date.now() },
  eventName: { type: String },
  payload: { type: this.payloadSchema },
});

CqrsDomainSchema.pre("save", function (next) {
  this._id = this.get("id");

  next();
});
