import { IEventHandler, EventsHandler } from "@nestjs/cqrs";
import { SampleCreatedEvent } from "./event-stream-created.evt";
import { SampleEventRepository } from "../repository/sample.event.repository";
import { NotifierClient } from "../../mgs-sender/notifier.client";

const clc = require("cli-color");

@EventsHandler(SampleCreatedEvent)
export class SampleCreatedEventHandler
  implements IEventHandler<SampleCreatedEvent> {
  constructor(
    private readonly repository: SampleEventRepository,
    private readonly notifierClient: NotifierClient
  ) {}

  handle(event: SampleCreatedEvent) {
    return this.repository
      .createEvent(event)
      .then((response) => {
        if (response && response.id && response.payload) {
          this.notifierClient.sendNotify(
            "info",
            event.baseEventStream.returnMessagePattern,
            response.payload
          );
        }

        return response;
      })
      .catch((error) => {
        this.notifierClient.sendNotify(
          "error",
          event.baseEventStream.returnMessagePattern,
          error
        );

        return error;
      });
  }
}
