import { Injectable, BadRequestException } from "@nestjs/common";
import { CommandBus } from "@nestjs/cqrs";
import { CreateSampleCommand } from "./commands/impl/create-sample.cmd";
import { UpdateSampleCommand } from "./commands/impl/update-sample.cmd";
import { DeleteSampleCommand } from "./commands/impl/delete-sample.cmd";
import { SampleQueryRepository } from "../sample.queryside/repository/sample.query.repository";
import { isNullOrUndefined } from "util";
import { CreateSampleDto } from "./dto/sample.dto";
import { MsxLoggerService } from "../logger/logger.service";
import { CommonConst } from "../shared/constant";
import { CodeGenerateService } from "../code-generate/service";
import { Action } from "../shared/enum/action.enum";
import { ErrorConst } from "../shared/constant/error.const";

const uuid = require("uuid");
const clc = require("cli-color");

@Injectable()
export class SampleDomainService {
  private readonly context = SampleDomainService.name;
  private commandId: string;
  constructor(
    private readonly commandBus: CommandBus,
    private readonly loggerService: MsxLoggerService // private readonly codeGenerateService: CodeGenerateService, // private readonly queryRepository: SampleQueryRepository
  ) {}

  async createSample(user: any, dto: CreateSampleDto, actionName: string) {
    this.loggerService.log(this.context, clc.green("create service"));
    const model: any = { ...dto };
    if (model.id) {
      delete model.id;
    }
    this.commandId = uuid.v4();
    model.modifiedBy = user.id;
    model.createdBy = user.id;
    await this.executeCommand(Action.CREATE, actionName, this.commandId, model);
    return { id: this.commandId, code: model.code };
  }

  async updateSample(user: any, dto: CreateSampleDto, actionName: string) {
    this.loggerService.log(this.context, clc.green("update service"));
    const model: any = { ...dto };
    this.commandId = uuid.v4();
    model.modifiedBy = user.id;
    await this.executeCommand(Action.UPDATE, actionName, this.commandId, model);
    return { id: dto.id };
  }

  async deleteSample(user: any, id: string, actionName: string) {
    this.loggerService.log(this.context, clc.green("delete service"));
    const model: any = { id };
    this.commandId = uuid.v4();
    model.modifiedBy = user.id;
    await await this.executeCommand(
      Action.DELETE,
      actionName,
      this.commandId,
      model
    );
    return { id: id };
  }

  private async executeCommand(
    action: string,
    actionName: string,
    commandId: string,
    item: any
  ) {
    let commandObject = null;
    switch (action) {
      case Action.CREATE:
        commandObject = new CreateSampleCommand(actionName, commandId, item);
        break;
      case Action.UPDATE:
        commandObject = new UpdateSampleCommand(actionName, commandId, item);
        break;
      case Action.DELETE:
        commandObject = new DeleteSampleCommand(actionName, commandId, item);
        break;
      default:
        break;
    }

    return await this.commandBus
      .execute(commandObject)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
}
