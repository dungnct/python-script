import { Controller, Get, Param, Query, UseGuards } from "@nestjs/common";
import { SampleQueryService } from "./service";
import { LoggingInterceptor } from "../../common/interceptors/logging.interceptor";
import { RolesGuard } from "../../common/guards/roles.guard";
import { AuthGuard } from "@nestjs/passport";
import { ACGuard, UseRoles } from "nest-access-control";
import { Usr } from "../shared/services/user/decorator/user.decorator";
import { PermissionEnum } from "../shared/enum/permission.enum";
import { ValidationPipe } from "../../common/pipes/validation.pipe";

@Controller("v1/sample")
@UseGuards(AuthGuard("jwt"))
export class SampleQueryController {
  constructor(private readonly sampleService: SampleQueryService) {}

  @UseGuards(RolesGuard, ACGuard)
  @UseRoles({
    resource: PermissionEnum.SAMPLE_GET_ALL, // => feature name
    action: "read",
    possession: "own",
  })
  @Get()
  findAll(@Usr() user, @Query() query: any) {
    return this.sampleService.findAll(query);
  }

  @UseGuards(RolesGuard, ACGuard)
  @UseRoles({
    resource: PermissionEnum.SAMPLE_GET_ID, // => feature name
    action: "read",
    possession: "own",
  })
  @Get(":id")
  findById(@Param("id") id: string) {
    return this.sampleService.findById(id);
  }
}
