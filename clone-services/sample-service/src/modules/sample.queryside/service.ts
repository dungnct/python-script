import { Injectable } from "@nestjs/common";
import { SampleQueryRepository } from "./repository/sample.query.repository";

@Injectable()
export class SampleQueryService {
  constructor(private readonly repository: SampleQueryRepository) {}
  async findById(id: string) {
    return await this.repository.findOne({ id });
  }
  async findAll(query) {
    const _query: any = {};
    if (query._fields) {
      _query._fields = query._fields;
    }
    if (query.q) {
      _query.$or = [
        { name: { $regex: query.q, $options: "i" } },
        { code: { $regex: query.q, $options: "i" } },
      ];
    }
    if (query["page"] && query["pageSize"]) {
      const page = parseInt(query["page"]) || 1;
      const pageSize = parseInt(query["pageSize"]) || 10;
      _query.page = page;
      _query.pageSize = pageSize;
      _query.isPaging = true;
      return Promise.all([
        await this.repository.findAll(_query),
        await this.repository.countAll(_query),
      ]).then((res) => ({
        rows: res[0],
        total: res[1],
        page,
        pageSize,
        totalPages: Math.floor((res[1] + pageSize - 1) / pageSize),
      }));
    } else {
      return await this.repository.findAll(_query);
    }
  }
}
