import { Document } from "mongoose";
import { ISample } from "../../shared/services/sample/interfaces/interface";

export interface ISampleDocument extends Document, ISample {
  id: string;
  description: string;
}
