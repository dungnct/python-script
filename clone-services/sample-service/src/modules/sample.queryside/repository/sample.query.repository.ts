import { Model } from "mongoose";
import { Inject, Injectable } from "@nestjs/common";
import { QueryAggregateModel } from "../models/query-aggregate.model";
import { ISampleDocument } from "../interfaces/document.interface";
import { IResult } from "../../shared/interfaces/result.interface";
import _ = require("lodash");
import { CommonConst } from "../../shared/constant/index";
import { EmployeeClient } from "../../mgs-sender/employee.client";
import { StsClient } from "../../mgs-sender/sts.client";
import { CustomerClient } from "../../mgs-sender/customer.client";
import { CommonUtils } from "../../shared/classes/class-utils";

@Injectable()
export class SampleQueryRepository {
  constructor(
    @Inject(CommonConst.SAMPLE_QUERY_MODEL_TOKEN)
    private readonly readModel: Model<ISampleDocument> // private readonly employeeClient: EmployeeClient,
  ) {}

  async findAggregateModelById(id: string): Promise<QueryAggregateModel> {
    return await this.readModel
      .findOne({ id })
      .exec()
      .then((response) => {
        return new QueryAggregateModel(id);
      })
      .catch((exp) => {
        return exp;
      });
  }

  async findOne(query, projection = {}): Promise<ISampleDocument> {
    return await this.readModel.findOne(query, projection);
  }
  async findAll(query): Promise<ISampleDocument[]> {
    let project: any = { _id: 0 };
    if (!_.isEmpty(query._fields)) {
      const fields = query._fields.split(",");
      fields.forEach((f) => {
        project[f.trim()] = 1;
      });
      delete query._fields;
    }
    let sort: any = {
      createdDate: -1,
    };
    if (!_.isEmpty(query.sort)) {
      sort = CommonUtils.transformSort(query.sort) || {
        createdDate: -1,
      };
    }
    if (query.isPaging) {
      const page = query.page;
      const pageSize = query.pageSize;
      delete query.isPaging;
      delete query.page;
      delete query.pageSize;
      return await this.readModel
        .find(query, project)
        .sort(sort)
        .skip(page * pageSize - pageSize)
        .limit(pageSize);
    }
    return await this.readModel.find(query, project).sort(sort);
  }
  async countAll(query) {
    delete query.isPaging;
    delete query.page;
    delete query.pageSize;
    return await this.readModel.countDocuments(query);
  }
  async find(query, projection = {}): Promise<ISampleDocument[]> {
    return await this.readModel.find(query, projection);
  }
  async create(readmodel): Promise<ISampleDocument> {
    return await this.readModel
      .create(readmodel)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
  async update(model): Promise<ISampleDocument> {
    console.log(model);
    return await this.readModel
      .update({ id: model.id }, model)
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
  async delete(model): Promise<any> {
    return await this.readModel
      .deleteOne({ id: model.id })
      .then((response) => {
        return response;
      })
      .catch((error) => {
        return error;
      });
  }
}
