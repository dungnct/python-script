import { IEvent } from "@nestjs/cqrs";
import { CommandModel } from "../../shared/eventStream/models/command.model";

export class SampleQueryUpdatedEvent implements IEvent {
  constructor(public readonly commandModel: CommandModel) {}
}
