import { IEventHandler, EventsHandler } from "@nestjs/cqrs";
import { SampleQueryCreatedEvent } from "./sample.query-created.evt";
import { SampleQueryUpdatedEvent } from "./sample.query-updated.evt";
import { SampleQueryDeletedEvent } from "./sample.query-deleted.evt";
import { SampleQueryRepository } from "../repository/sample.query.repository";
import { MsxLoggerService } from "../../../modules/logger/logger.service";

const clc = require("cli-color");

@EventsHandler([
  SampleQueryCreatedEvent,
  SampleQueryUpdatedEvent,
  SampleQueryDeletedEvent,
])
export class SampleQueryEventHandler
  implements
    IEventHandler<SampleQueryCreatedEvent>,
    IEventHandler<SampleQueryUpdatedEvent>,
    IEventHandler<SampleQueryDeletedEvent> {
  private readonly context = SampleQueryEventHandler.name;
  constructor(
    private readonly repository: SampleQueryRepository,
    private readonly loggerService: MsxLoggerService
  ) {}

  handle(
    event:
      | SampleQueryCreatedEvent
      | SampleQueryUpdatedEvent
      | SampleQueryDeletedEvent
  ) {
    if (event && event instanceof SampleQueryCreatedEvent) {
      this.createdHandler(event.commandModel);
    } else if (event && event instanceof SampleQueryUpdatedEvent) {
      this.updatedHandler(event.commandModel);
    } else if (event && event instanceof SampleQueryDeletedEvent) {
      this.deletedHandler(event);
    }
  }

  private createdHandler(event) {
    this.loggerService.log(
      this.context,
      clc.yellowBright("Async Sample Created Query Event Handler...")
    );
    this.repository.create(event);
  }

  private updatedHandler(event) {
    this.loggerService.log(
      this.context,
      clc.yellowBright("Async Sample Update Query Event Handler...")
    );
    this.repository.update(event);
  }

  private deletedHandler(event) {
    this.loggerService.log(
      this.context,
      clc.yellowBright("Async Sample Delete Query Event Handler...")
    );
    this.repository.delete(event);
  }
}
