import { IEvent } from "@nestjs/cqrs";
import { CommandModel } from "../../shared/eventStream/models/command.model";

export class SampleQueryCreatedEvent implements IEvent {
  constructor(public readonly commandModel: CommandModel) {}
}
