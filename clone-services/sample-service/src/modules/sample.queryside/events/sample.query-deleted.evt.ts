import { IEvent } from "@nestjs/cqrs";

export class SampleQueryDeletedEvent implements IEvent {
  constructor(public readonly id: string) {}
}
