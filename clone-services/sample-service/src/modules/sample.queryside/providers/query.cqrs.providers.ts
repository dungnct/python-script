import { Connection } from "mongoose";
import { SampleQuerySchema } from "../schemas/sample.query.schema";
import { CommonConst } from "../../shared/constant";

export const QueryProviders = [
  {
    provide: CommonConst.SAMPLE_QUERY_MODEL_TOKEN,
    useFactory: (connection: Connection) =>
      connection.model(CommonConst.SAMPLE_COLLECTION, SampleQuerySchema),
    inject: [CommonConst.QUERY_CONNECTION_TOKEN],
  },
];
