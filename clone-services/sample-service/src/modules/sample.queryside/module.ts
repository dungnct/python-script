import { CqrsModule } from "@nestjs/cqrs";
import { SampleQueryCommandHandlers } from "./commands/handlers";
import { SampleQueryEventHandlers } from "./events";
import { SampleQueryController } from "./controller";
import { SampleQueryService } from "./service";
import { SampleQueryRepository } from "./repository/sample.query.repository";
import { QueryDatabaseModule } from "../database/query/query.database.module";
import { QueryProviders } from "./providers/query.cqrs.providers";
import { Module } from "@nestjs/common";
import { AuthModule } from "../auth/auth.module";
import { LoggerModule } from "../.././modules/logger/logger.module";
import { MgsSenderModule } from "../mgs-sender/mgs-sender.module";

@Module({
  imports: [
    CqrsModule,
    QueryDatabaseModule,
    AuthModule,
    LoggerModule,
    MgsSenderModule,
  ],
  controllers: [SampleQueryController],
  providers: [
    SampleQueryService,
    SampleQueryRepository,
    ...QueryProviders,
    ...SampleQueryCommandHandlers,
    ...SampleQueryEventHandlers,
  ],
  exports: [SampleQueryRepository, SampleQuerySideModule, SampleQueryService],
})
export class SampleQuerySideModule {}
