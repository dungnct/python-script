import * as mongoose from "mongoose";
import uuid = require("uuid");

export const SampleQuerySchema = new mongoose.Schema({
  _id: { type: String },
  id: { type: String, default: uuid.v4, index: true },
  code: { type: String },
  name: { type: String },

  description: { type: String, default: "" },
  modifiedBy: String,
  createdDate: { type: Date, default: () => Date.now() },
  modifiedDate: { type: Date, default: () => Date.now() },
  eventName: { type: String },
  actionName: { type: String },
});

SampleQuerySchema.pre("save", function (next) {
  this._id = this.get("id");
  next();
});

// =====
