import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { DeleteSampleQueryCommand } from "../impl/delete-sample.query.cmd";
import { SampleQueryRepository } from "../../repository/sample.query.repository";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";

const clc = require("cli-color");

@CommandHandler(DeleteSampleQueryCommand)
export class DeleteSampleQueryCommandHandler
  implements ICommandHandler<DeleteSampleQueryCommand> {
  private readonly context = DeleteSampleQueryCommandHandler.name;
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  constructor(
    private readonly repository: SampleQueryRepository,
    private readonly publisher: EventPublisher,
    private readonly loggerService: MsxLoggerService
  ) {}

  async execute(command: DeleteSampleQueryCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Delete query ${this.aggregateName} cmd ...`)
    );
    const { messagePattern, commandModel } = command;
    const cmdPublisher = this.publisher.mergeObjectContext(
      await this.repository.findAggregateModelById(commandModel.id)
    );

    cmdPublisher.deleteItem(commandModel.id);
    cmdPublisher.commit();
  }
}
