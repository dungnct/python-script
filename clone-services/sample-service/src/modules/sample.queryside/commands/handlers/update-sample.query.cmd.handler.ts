import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { UpdateSampleQueryCommand } from "../impl/update-sample.query.cmd";
import { SampleQueryRepository } from "../../repository/sample.query.repository";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";

const clc = require("cli-color");

@CommandHandler(UpdateSampleQueryCommand)
export class UpdateSampleQueryCommandHandler
  implements ICommandHandler<UpdateSampleQueryCommand> {
  private readonly context = UpdateSampleQueryCommandHandler.name;
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  constructor(
    private readonly repository: SampleQueryRepository,
    private readonly publisher: EventPublisher,
    private readonly loggerService: MsxLoggerService
  ) {}

  async execute(command: UpdateSampleQueryCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Update query ${this.aggregateName} cmd ...`)
    );

    const { messagePattern, id, commandModel } = command;

    const cmdPublisher = this.publisher.mergeObjectContext(
      await this.repository.findAggregateModelById(id)
    );

    // const msg = this.aggregateName + '.' + messagePattern;
    cmdPublisher.updateItem(commandModel);
    cmdPublisher.commit();
  }
}
