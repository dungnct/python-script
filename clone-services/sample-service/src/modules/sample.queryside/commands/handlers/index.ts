import { CreateSampleQueryCommandHandler } from "./create-sample.query.cmd.handler";
import { UpdateSampleQueryCommandHandler } from "./update-sample.query.cmd.handler";
import { DeleteSampleQueryCommandHandler } from "./delete-sample.query.cmd.handler";

export const SampleQueryCommandHandlers = [
  CreateSampleQueryCommandHandler,
  UpdateSampleQueryCommandHandler,
  DeleteSampleQueryCommandHandler,
];
