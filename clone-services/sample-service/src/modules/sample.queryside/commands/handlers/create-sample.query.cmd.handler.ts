import { EventPublisher, ICommandHandler, CommandHandler } from "@nestjs/cqrs";
import { CreateSampleQueryCommand } from "../impl/create-sample.query.cmd";
import { SampleQueryRepository } from "../../repository/sample.query.repository";
import { CommonConst } from "../../../shared/constant/index";
import { MsxLoggerService } from "../../../logger/logger.service";

const clc = require("cli-color");

@CommandHandler(CreateSampleQueryCommand)
export class CreateSampleQueryCommandHandler
  implements ICommandHandler<CreateSampleQueryCommand> {
  private readonly context = CreateSampleQueryCommandHandler.name;
  private readonly aggregateName: string = CommonConst.SAMPLE_AGGREGATE_NAME;
  constructor(
    private readonly repository: SampleQueryRepository,
    private readonly publisher: EventPublisher,
    private readonly loggerService: MsxLoggerService
  ) {}

  async execute(command: CreateSampleQueryCommand) {
    this.loggerService.log(
      this.context,
      clc.yellowBright(`Async Create query ${this.aggregateName} cmd ...`)
    );
    const { messagePattern, id, commandModel } = command;
    const cmdPublisher = this.publisher.mergeObjectContext(
      await this.repository.findAggregateModelById(id)
    );
    cmdPublisher.addItem(commandModel);
    cmdPublisher.commit();
  }
}
