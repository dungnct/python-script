import { ICommand } from "@nestjs/cqrs";
import { CommandModel } from "../../../shared/eventStream/models/command.model";

export class CreateSampleQueryCommand implements ICommand {
  constructor(
    public readonly messagePattern: string,
    public readonly id: string,
    public readonly commandModel: CommandModel
  ) {}
}
