import { AggregateRoot } from "@nestjs/cqrs";
import { SampleQueryCreatedEvent } from "../events/sample.query-created.evt";
import { SampleQueryUpdatedEvent } from "../events/sample.query-updated.evt";
import { SampleQueryDeletedEvent } from "../events/sample.query-deleted.evt";
import { CommandModel } from "../../shared/eventStream/models/command.model";

export class QueryAggregateModel extends AggregateRoot {
  constructor(private readonly id: string) {
    super();
  }

  addItem(commandModel: CommandModel) {
    this.apply(new SampleQueryCreatedEvent(commandModel));
  }

  updateItem(commandModel: CommandModel) {
    this.apply(new SampleQueryUpdatedEvent(commandModel));
  }

  deleteItem(id: string) {
    this.apply(new SampleQueryDeletedEvent(id));
  }
}
