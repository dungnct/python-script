# Requisitic - Environment 

Node:
https://nodejs.org/en/download/

Docker:
https://www.docker.com/community-edition#/download
Please choose Mac or Windows

After install Docker, In case you run on Windows,
Option Power Shell, run script:
`set COMPOSE_CONVERT_WINDOWS_PATHS=1`

Optional for manage easier
https://kitematic.com/

# build insfrastructure
docker-compose -f docker-compose-infra.yml build
docker-compose -f docker-compose-infra.yml up -d
# or build all services
docker-compose build