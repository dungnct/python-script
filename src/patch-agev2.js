
const Tranx = db.getMongo().getDB('th-msx-property-readmodel').getCollection('primary-transactions');//ycdch,ycdco
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	Tranx.update(
    {
          $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /\d{4}-\d{1,2}-\d{1,2}/
      },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate",
                {
                  "$toDate": "$customer.info.birthday",
                }
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )
    Tranx.update(
      {
        "customer.info.onlyYear": { $eq: true },
        "customer.info.birthdayYear" : /^\d{4}$/
      },
          [{$set: {"customer.info.age": {
            "$divide": [
              {
                "$subtract": [
                  "$createdDate",
                  {
                    "$toDate": {
                      "$concat": [
                        "$customer.info.birthdayYear",
                        "-01-01"
                      ]
                    },
                  }
                ]
              },
              (365.25 * 24*60*60*1000)
            ]
          }}}], 
          {multi: true}
      )
	Tranxs = Tranx.find(
    {
          $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /Coordinated Universal Time/
      }).toArray();
	  log(Tranxs.length);
    for (let i = 0; i < Tranxs.length; i++) {
		const tranx = Tranxs[i];
		log(tranx.id);
		let birthday = new Date(tranx.customer.info.birthday);
		log(birthday);
		const age = (tranx.createdDate - birthday)/(365.25 * 24*60*60*1000);
		log(age);
		Tranx.updateOne({id: tranx.id}, {$set: {"customer.info.age": age}});
    }
}
main();