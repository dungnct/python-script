import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))


Contract = clientsource['msx-contract-readmodel']['contracts']
Property = clientsource['msx-property-readmodel']['property-units_copy_dung']




def GetPropertyContract():
    query = {"propertyUnitId":{"$ne":None}}
    select = {"propertyUnitId": 1,"dealtDeposit":1}
    docs = Contract.find(query, select)
    return docs

def SetPropertyStatus(property):
    query = { "id" : property["propertyUnitId"]}
    if  property["dealtDeposit"] == None:
        new = { "$set": {"propertyStatus" : None}}
        Property.update_many(query,new)
    if  property["dealtDeposit"] != None:
        new1 = { "$set": {"propertyStatus" : "deposit"}}
        Property.update_many(query,new1)

def main():
    propertys = GetPropertyContract()
    for property in propertys:
       SetPropertyStatus(property)  
main()