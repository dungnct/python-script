import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))


MatchingUnit=clientsource["msx-matching-readmodel"]["unit-attributes-test"]
MatchingUnitAttribures=clientsource["msx-matching-readmodel"]["unit-attributes-test"]
MathingDemand=clientsource["msx-matching-readmodel"]["demand-attributes-test"]
MathingDemandAttribures=clientsource["msx-matching-readmodel"]["demand-attributes-test"]


def GetUnitAttributes():
    docs = MatchingUnit.find({'key':"location"},{'unitId': 1 , 'text': 1})
    return docs

def GetDemandAttributes():
    docs = MathingDemand.find({'key':"location"},{'demandId': 1 , 'value': 1})
    return docs
    
    
def UpdateAddressUnitAttributes(unit):
    query  = {"unitId" : unit["unitId"] }
    querylocation  = {"unitId" : unit["unitId"] , "key" : "location"}
    new = { '$set' : { 'address' : unit['text'] } }
    print(unit["unitId"])
    MatchingUnitAttribures.update_many(query,new)
    MatchingUnitAttribures.delete_one(querylocation)
        
def UpdateAddressDemandAttribures(demand):
    query  = {"demandId" : demand["demandId"] }
    querylocation  = {"demandId" : demand["demandId"] , "value" : "location"}
    new = { '$set' : { 'address' : demand['value'] } }
    print(demand["demandId"])
    MathingDemandAttribures.update_many(query,new)
    MathingDemandAttribures.delete_one(querylocation)

def main():
    units = GetUnitAttributes()
    for unit in units:
       UpdateAddressUnitAttributes(unit)
    demands =  GetDemandAttributes()
    for demand in demands:
        UpdateAddressDemandAttribures(demand)
main()