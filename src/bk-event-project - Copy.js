// const projectId = '80b6d4ad-906b-4e70-8b4c-eb2250fd9ca8';//Gem City 13/06/2020 thuc hanh
const projectId = 'f105fd59-ab80-459e-853c-7c6a2c029008';//DA3 - GEM
const Tranx = db.getMongo().getDB('msx-property-readmodel').getCollection('primary-transactions');//ycdch,ycdco
const EventProject = db.getMongo().getDB('msx-property-readmodel').getCollection('event-projects');//ycdch,ycdco
const Unit = db.getMongo().getDB('msx-property-readmodel').getCollection('property-units');//ycdch,ycdco
const Project = db.getMongo().getDB('msx-property-readmodel').getCollection('projects');//ycdch,ycdco
const Transaction = db.getMongo().getDB('msx-transaction-readmodel').getCollection('transactions');//phieuthu
function log(obj){
	print(JSON.stringify(obj));
}

function deleteProject(projectId) {
	Project.deleteMany({id: projectId});
}
function deleteEventProject(projectId) {
	EventProject.deleteMany({projectId: projectId});
}
function deleteTranx(projectId) {
	Tranx.deleteMany({'project.id': projectId});
}
function deleteTransaction(projectId) {
	Transaction.deleteMany({'propertyTicket.project.id': projectId});
}
function deleteUnit(projectId) {
	Unit.deleteMany({'project.id': projectId});
}

function main(){
	deleteProject(projectId);
	deleteEventProject(projectId);
	deleteTranx(projectId);
	deleteUnit(projectId);
	deleteTransaction(projectId);
	// insertProject(projectId);
	// insertEventProject(projectId);
	// insertTranx(projectId);
	// insertUnit(projectId);
	// insertTransaction(projectId);
}
main();