const dbProperty = 'msx-property-readmodel';
const projectId = '7e8fa6af-4abd-4c53-9129-bac2937ac183';
const colProperty = 'primary-transactions';
const colEmployee = 'employees';
const TicketCol = db.getMongo().getDB(dbProperty).getCollection(colProperty);//ticket collection
const EmployeeCol = db.getMongo().getDB(dbProperty).getCollection(colEmployee); //orgchart collection
const employeeIds = [
	"d3320874-a6cc-439a-aec3-44c32a84286a",
	"6e424d74-28cb-4fbc-aeb8-c85a3dc65ddb",
	"2ff9eec6-0c41-4853-a496-4f7583f04adb",
]


function log(obj){
	print(JSON.stringify(obj));
}

function patchEmployeeTicket(){
	const employees = EmployeeCol.find({id: {$in: employeeIds}}, {
		'pos.code': 1, 'pos.parentId':1, 'pos.name': 1, 'pos.id': 1, 'pos.type': 1, 'pos.staffIds': 1, 'pos.managerId': 1,
		id: 1, name: 1, staffIds: 1, code: 1, email: 1, phone: 1, images: 1, workingAt: 1, managerAt: 1
	}).toArray();
	
	let list = TicketCol.find(
		{
		'project.id': projectId,
		'employee.id': {$in: employeeIds}
	}).toArray();
	for (let i = 0; i < list.length; i++) {
		const ticket = list[i];
		const employee = employees.find(emp => emp.id === ticket.employee.id);
		log(ticket.id);
    ticket.employee = employee;
    ticket.pos = employee.pos;
		TicketCol.updateOne({
      'id': ticket.id
    }, {
      $set: {employee: employee}
    });
	}
}


function main(){
	patchEmployeeTicket();
}
main();