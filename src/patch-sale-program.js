const projectId = '909d82bb-c1a6-4d6d-815e-121637ef1bc4';
const salesProgram = {id: 'ff248677-0cd5-4218-a643-020361d00454', name: 'Opal Skyline Đợt 1'}
const Tranx = db.getMongo().getDB('th-msx-property-readmodel').getCollection('primary-transactions');//ycdch,ycdco
const Unit = db.getMongo().getDB('th-msx-property-readmodel').getCollection('property-units');
const EventProject = db.getMongo().getDB('th-msx-property-readmodel').getCollection('event-projects');

function log(obj){
	print(JSON.stringify(obj));
}
function main(){
	// 1. thêm salesProgram vào unit cũ
	Unit.updateMany({
		'project.id': projectId,
		}, {
			$set: {
				'salesProgram': salesProgram
				
			}
		}
	)
	// 2. thêm salesProgram vào primary-transactions cũ
	Tranx.updateMany({
		'project.id': projectId,
		'propertyUnit': {$ne: null},
		}, 
		{
			$set: {
				'propertyUnit.salesProgram': salesProgram
			}
		}
	)
	// 3. Đưa bảng điều khiển về đang ở giai đoạn 2
	EventProject.updateOne({
		projectId: projectId,
		'salesProgram.id': salesProgram.id
		},
		{
			$set: {
				stage: 2,
				priorityStatus: 'in_progress',
				stageStatus: 'in_progress',
				priority: 3,
				status: 'streaming'
			}
		}
    )
	// 4. Ấn kết thúc sự kiện ở bảng điều khiển
}
main();