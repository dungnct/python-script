const Orgchart = db.getMongo().getDB('spx-orgchart-readmodel').getCollection('orgcharts');
const Employee = db.getMongo().getDB('spx-employee-readmodel').getCollection('employees');
const EmployeeEvent = db.getMongo().getDB('spx-employee-eventstore').getCollection('events-employees');
const Code = db.getMongo().getDB('spx-employee-readmodel').getCollection('code-generates');
const EmployeeInCus = db.getMongo().getDB('spx-customer-readmodel').getCollection('employees');
const EmployeeInDemand = db.getMongo().getDB('spx-demand-readmodel').getCollection('employees');
const EmployeeInOrgchart = db.getMongo().getDB('spx-orgchart-readmodel').getCollection('employees');
const EmployeeInProperty = db.getMongo().getDB('spx-property-readmodel').getCollection('employees');
const EmployeeInSocial = db.getMongo().getDB('spx-social-readmodel').getCollection('employees');
const EmployeeInTrans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('employees');
const Tranx = db.getMongo().getDB('spx-property-readmodel').getCollection('primary-transactions');
const Trans = db.getMongo().getDB('spx-transaction-readmodel').getCollection('transactions');
function log(obj){
	print(JSON.stringify(obj));
}
function updateEmpPos(empId, pos) {
	Employee.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	// update employee other database
	EmployeeInCus.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	EmployeeInDemand.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	EmployeeInProperty.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	EmployeeInSocial.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	EmployeeInTrans.updateOne({id: empId}, {$set: {pos : pos, workingAt: pos.id}});
	
}

function updateEmpPosNull() {
	const POS_PROPCOM_TVV = '26f3c6a0-12f2-4a43-88f4-d9341a03b34a';
	const sanSpx = Orgchart.findOne({id: POS_PROPCOM_TVV});
	let query = {
		'$or': [
      {pos: null},
      {pos: {$exists: false}},
    ]
	};
	let model = {'$set': {pos: sanSpx, workingAt: sanSpx.id}};
	Employee.update(query, model, {multi: true});
	EmployeeInCus.update(query, model, {multi: true});
	EmployeeInDemand.update(query, model, {multi: true});
	EmployeeInProperty.update(query, model, {multi: true});
	EmployeeInSocial.update(query, model, {multi: true});
	EmployeeInTrans.update(query, model, {multi: true});
	
	const employees = Employee.find({'pos.id': POS_PROPCOM_TVV});
	const empIds = employees.map(emp => emp.id);
	log(empIds);
	Orgchart.updateOne({id: POS_PROPCOM_TVV}, {
		$addToSet: {
			staffIds: {$each: empIds}, 
			personalStaffIds: {$each: empIds}
		}});
		
	query = {
		'$or': [
      {workingAt: null},
      {workingAt: ''},
      {workingAt: {$exists: false}},
    ]
	};
	model = {'$set': {workingAt: sanSpx.id}};
	Employee.update(query, model, {multi: true});
	EmployeeInCus.update(query, model, {multi: true});
	EmployeeInDemand.update(query, model, {multi: true});
	EmployeeInProperty.update(query, model, {multi: true});
	EmployeeInSocial.update(query, model, {multi: true});
	EmployeeInTrans.update(query, model, {multi: true});
}
function updateEmpPosRefer(empId, pos) {
	Tranx.updateMany({'employee.id': empId}, {$set: {'employee.pos' : pos}});
	Trans.updateMany({'propertyTicket.employee.id': empId}, {$set: {'propertyTicket.employee.pos' : pos}});
	Trans.updateMany({'transaction.propertyTicket.employee.id': empId}, {$set: {'transaction.propertyTicket.employee.pos' : pos}});
}

function updateEmpPosAsReferral(){
	//find employee has referralPerson
	const employees = Employee.find({
		'$and': [
      {'referralPerson.id': {$nin: [null, '']}},
      {'referralPerson.id': {$exists: true}}
    ]
	}, {id: 1, code: 1, referralPerson: 1, pos: 1}).toArray();
	//update employee's pos same referralPerson's pos
	for (let i = 0; i < employees.length; i++) {
		const emp = employees[i];
		log('emp code : ' + emp.code);
		if(emp.referralPerson && emp.referralPerson.id && emp.referralPerson.id != ''){
			const referralPerson = Employee.findOne({id: emp.referralPerson.id}, {id: 1, code: 1, pos: 1});
			updateEmpPos(emp.id, referralPerson.pos);
			updateEmpPosRefer(emp.id, referralPerson.pos);
		}
	}
}
function main(){
	//update pos null as sanSpx
	updateEmpPosNull();
	updateEmpPosAsReferral();
}
main();