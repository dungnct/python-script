// const dbProperty = 'msx-property-readmodel';
// const projectId = '';
const Tranx = db.getMongo().getDB(dbProperty).getCollection('primary-transactions');//tranx collection
const Unit = db.getMongo().getDB(dbProperty).getCollection('property-units');//tranx collection

function log(obj){
	print(JSON.stringify(obj));
}

function patchUnitConfirmDate(){
	let list = Tranx.find({
		'historyStatus.status': 'POS_CONFIRM_LOCK',
		'propertyUnit.id': {$ne: null}
	}).toArray();
	for (let i = 0; i < list.length; i++) {
		const tranx = list[i];
		const historyStatus = tranx.historyStatus.find(h => h.status === 'POS_CONFIRM_LOCK');
		log({ 
			id: tranx.propertyUnit.id,
			'priorities.id' : tranx.id
		});
		log(historyStatus);
    Unit.findOneAndUpdate({ 
			id: tranx.propertyUnit.id,
			'priorities.id' : tranx.id
		},
		{ $set: {
			'priorities.$.unitConfirmDate': historyStatus.date
		}},
		{new: true}
		)
	}
}


function main(){
	patchUnitConfirmDate();
}
main();