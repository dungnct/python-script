const Ticket = db.getMongo().getDB('spx-property-readmodel').getCollection('primary-transactions');
const Unit = db.getMongo().getDB('spx-property-readmodel').getCollection('property-units');
function log(obj){
	print(JSON.stringify(obj));
}
function main(){
	const units = Unit.find({priorities: {$size: 1}}, {id: 1, 'priorities': 1}).toArray();
	
	for (let i = 0; i < units.length; i++) {
		const unit = units[i];
		log('unit id ' + unit.id + ', ticket id ' + unit.priorities[0].id)
		const ticket = Ticket.findOne({id: unit.priorities[0].id}, {id: 1, 'customer.personalInfo.phone': 1});
		unit.priorities[0].customerPhone = ticket.customer.personalInfo.phone;
		Unit.updateOne({id: unit.id}, {$set: {priorities: unit.priorities}});
	}
}
main();