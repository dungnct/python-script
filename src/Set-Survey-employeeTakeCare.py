import sys
import json
import pymongo
import shutil
from datetime import datetime, date

hostsource = sys.argv[1]
portsource = sys.argv[2]
hostdest = sys.argv[3]
portdest = sys.argv[4]
strFromDate = sys.argv[5]
strEndDate = sys.argv[6]

if strFromDate == "" or strEndDate == "":
    fromDate = datetime.strptime("1/1/1996","%d/%m/%Y")
    endDate = datetime.now()
else:
    fromDate = datetime.strptime(sys.argv[5], "%d/%m/%Y")
    endDate = datetime.strptime(sys.argv[6], "%d/%m/%Y")

clientsource = pymongo.MongoClient(hostsource, int(portsource))
clientdest = pymongo.MongoClient(hostdest, int(portdest))

DContractSource = clientsource['msx-contract-readmodel']['draft-contracts']
ContractSource = clientsource['msx-contract-readmodel']['draft-contracts']
SurveyDest = clientdest['msx-property-readmodel']['leads']

def GetProperty():
    SurProperty = clientsource["msx-property-readmodel"]["survey-property-units"]
    query = { "employeeTakeCare" : {"$eq":None}}
    select = {"id": 1}
    docs = SurProperty.find(query, select)
    return docs

def DraftContract():
    Draft = clientsource["msx-contract-readmodel"]["draft-contracts"]
    query = {"employeeTakeCare":{"$ne":None} , "propertyUnitId":{"$ne":None}}
    select = {"propertyUnitId": 1, "employeeTakeCare.id" : 1 , "employeeTakeCare.name" : 1 , "employeeTakeCare.email" : 1 , "employeeTakeCare.code" : 1 }
    docs = Draft.find(query, select)
    return docs

def Contract():
    Contract = clientsource["msx-contract-readmodel"]["contracts"]
    query = {"employeeTakeCare":{"$ne":None} , "propertyUnitId":{"$ne":None}}
    select = {"propertyUnitId": 1, "employeeTakeCare.id" : 1 , "employeeTakeCare.name" : 1 , "employeeTakeCare.email" : 1 , "employeeTakeCare.code" : 1 }
    docs = Contract.find(query, select)
    return docs

    
def Act(property):
    SurProperty = clientsource["msx-property-readmodel"]["survey-property-units"]
    docs = SurProperty.find({"_id":property["propertyUnitId"]})
    query = {"_id":property["propertyUnitId"]}
    new = { "$set": { "employeeTakeCare.id": property["employeeTakeCare"]["id"] , "employeeTakeCare.name": property["employeeTakeCare"]["name"] , "employeeTakeCare.email": property["employeeTakeCare"]["email"] , "employeeTakeCare.code": property["employeeTakeCare"]["code"] }}
    SurProperty.update_many(query,new)





def main():
    data = DraftContract()
    for property in data:
       Act(property)
    data2 = Contract()
    for property2 in data2:
       Act(property2)
    
#    data2 = GetCustomer2()
#    for user2 in data2:
#       Lead2(user2)
#    data3 = GetCustomer3()
#    for user3 in data3:
#       Lead3(user3)
#    data4 = GetCustomer4()
#    for user4 in data4:
#       Lead4(user4)
main()