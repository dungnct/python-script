const CustomerTable = db
  .getMongo()
  .getDB("msx-demand-readmodel")
  .getCollection("demand-customers");
function log(obj) {
  print(JSON.stringify(obj));
}
// production modifiedBy
function countPhoneDuplicate(arr) {
  return CustomerTable.aggregate([
    {
        $group: {
            _id: {
                createdBy: '$createdBy', 
                phone: '$personalInfo.phone'
            },
            countPhoneDuplicate: {"$sum": 1}
        }
    },
    {
        $match: {countPhoneDuplicate: {$gt: 1}}
    },
    {
       $lookup:
         {
           from: 'employees',
           localField: '_id.createdBy',
           foreignField: 'id',
           as: 'employee'
         }
    },
    {$unwind: '$employee'},
    {
        $project: {
            employeeEmail: '$employee.email',
            customerPhone: '$_id.phone',
            countPhoneDuplicate: 1,
            _id: 0
        }
    }
    ]).toArray();
}

function identityDuplicate() {
  return CustomerTable.aggregate([
    {
        $group: {
            _id: {
                createdBy: '$createdBy', 
                cusPhone: '$personalInfo.phone'
            },
            countPhoneDuplicate: {"$sum": 1}
        }
    },
    {
        $match: {countPhoneDuplicate: {$gt: 1}}
    },
    {
       $lookup:
         {
           from: 'employees',
           localField: '_id.createdBy',
           foreignField: 'id',
           as: 'employee'
         }
    },
    {$unwind: '$employee'},
    {
        $project: {
            employeeEmail: '$employee.email',
            cusPhone: '$_id.cusPhone',
            countPhoneDuplicate: 1,
            _id: 0
        }
    }
    ]).toArray();
}

function phoneDuplicate() {
  return CustomerTable.aggregate([
    {
      $addFields: {
          cusIdentity: '$identities.value',
      }
    },
    {$unwind: '$cusIdentity'},
    {
        $group: {
            _id: {
                createdBy: '$createdBy', 
                cusIdentity: '$cusIdentity'
            },
            countIdentityDuplicate: {"$sum": 1}
        }
    },
    {
        $match: {countIdentityDuplicate: {$gt: 1}}
    },
    {
      $lookup:
        {
          from: 'employees',
          localField: '_id.createdBy',
          foreignField: 'id',
          as: 'employee'
        }
    },
    {$unwind: '$employee'},
    {
        $project: {
            employeeEmail: '$employee.email',
            cusIdentity: '$_id.cusIdentity',
            countIdentityDuplicate: 1,
            _id: 0
        }
    }
    ]).toArray();
}

function main() {
  const arrPhoneDuplicate = phoneDuplicate();
  const arrIdentityDuplicate = identityDuplicate();
  let arr3 = arrPhoneDuplicate.map((item) => Object.assign({}, item, arrIdentityDuplicate.find(i => i.employeeEmail === item.employeeEmail)));
  // arr3.forEach((item) => log(item));
  log(arr3);
}
main();
