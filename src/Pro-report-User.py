from openpyxl import Workbook, load_workbook
import sys
from datetime import datetime, date
import json
import pymongo
import pprint
from unidecode import unidecode
import requests
import shutil
import openpyxl
inputFile = sys.argv[1]
dbhost = sys.argv[2]
dbport = sys.argv[3]
outputFile = sys.argv[4]
strFromDate = sys.argv[5]
strEndDate = sys.argv[6]
#intput = sys.argv[7]
if strFromDate == "" or strEndDate == "":
    fromDate = datetime.strptime("1/1/1996 00:00:00", "%d/%m/%Y %H:%M:%S")
    endDate = datetime.now('Asia/Ho_Chi_Minh')
else:
    fromDate = datetime.strptime(sys.argv[5], "%d/%m/%Y")
    endDate = datetime.strptime(sys.argv[6], "%d/%m/%Y")
print("fromDate ", fromDate)
print("endDate ", endDate)
client = pymongo.MongoClient(dbhost, int(dbport))
rowBegin=3
shutil.copy(inputFile, outputFile)

#Reading a workbook from file
#book = openpyxl.load_workbook(input)
#sheet = book.active
#row = sheet.max_row
#creating a workbook from file
myWorkbook=load_workbook(outputFile)
sheetUser=myWorkbook["User"]

######
# BEGIN REPORT USER
#####
userTemplate = {
    "stt" : 0,
    "manv" : "",
    "name" : "",
    "ttgd" : "",
    "firstLoginDate" : "",
    "lastLoginDate" : "",
    "listLogin": "",
    "numLogin": 0,
    "numInput": 0,
    "numInputE":0,
    "numDemandBroker":0,
    "numDemand": 0,
    "numConsignment": 0,
    "numConsignmentBroker":0,
    "numContractApprove": 0,
    "numCustomerInput": 0,
    "numCallTo": 0,
    "numCallFrom": 0,
    "numCusConsign":0,
    "numCusDemand":0,
    "numProDcontract":0,
    "numProContract":0,
    "numPro":0
}

userColumnIndex = {
    "stt" : 1,
    "manv" : 2,
    "name" : 3,
    "ttgd" : 4,
    "firstLoginDate" : 5,
    "lastLoginDate" : 6,
    "listLogin": 7,
    "numLogin": 8,
    "numInput": 9,
    "numInputE":10,
    "numDemandBroker":11,
    "numDemand": 12,
    "numConsignmentBroker":13,
    "numConsignment": 14,
    "numContractApprove": 15,
    "numCustomerInput": 16,
    "numCallTo": 17,
    "numCallFrom":18,
    "numCusConsign":19,
    "numCusDemand":20,
    "numProDcontract":21,
    "numProContract":22,
    "numPro":23
}
    
def getListAccount():
    db = client["msx-employee-readmodel"]
    dbCol = db["employees"]
    query = {"active": True , "pos" : {"$ne":None}}
    select = {"id": 1, "code" : 1, "name": 1, "pos.id" : 1, "pos.code" : 1, "pos.name" : 1 , "email": 1}
    docs = dbCol.find(query, select)
    return docs
 
def getNumDemandCreated(id):
    db = client["msx-demand-eventstore"]
    dbCol = db["events-demands"]
    query = {"payload.employee.id": id, "eventName" : "demandCreated","payload.resource":"employee","commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numDemand = dbCol.find(query).count()
    return numDemand

def getNumDemandW(id):
    db = client["msx-demand-eventstore"]
    dbCol = db["events-demands"]
    query = {"payload.employee.id": id, "eventName" : "demandCreated", "payload.resource":"webbroker" ,"commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numDemandW = dbCol.find(query).count()
    return numDemandW    

def getNumCusDemand(id):
    db = client["msx-demand-eventstore"]
    dbCol = db["events-demands"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "demandCreated","commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numCusDemand = dbCol.find(query).count()
    return numCusDemand
    
def getNumConsignmentCreated(id):
    db = client["msx-property-eventstore"]
    dbCol = db["events-consignments"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "consignmentCreated" , "payload.resource":"employee","commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numConsignment = dbCol.find(query).count()
    return numConsignment

def getNumConsignmentW(id):
    db = client["msx-property-eventstore"]
    dbCol = db["events-consignments"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "consignmentCreated" , "payload.resource":"webbroker","commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numConsignmentW = dbCol.find(query).count()
    return numConsignmentW   

def getNumCustomerConsign(id):
    db = client["msx-property-eventstore"]
    dbCol = db["events-consignments"]
    query = {"payload.employeeTakeCare.id": id, "payload.status" : "done" ,"commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    numCusConsign = dbCol.find(query).count()
    return numCusConsign
       
def getNumContractCreated(id):
    db = client["msx-contract-eventstore"]
    dbCol = db["events-contracts"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "contractCreated","commitStamp" : {"$gte" : fromDate, "$lte" : endDate} }
    numContract = dbCol.find(query).count()
    return numContract

   
def getNumContractApproved(id):
    db = client["msx-contract-eventstore"]
    dbCol = db["events-contracts"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "contractApproved" ,"commitStamp" : {"$gte" : fromDate, "$lte" : endDate} }
    numContract = dbCol.find(query).count()
    return numContract

def getNumProDcontract(id):
    db = client["msx-contract-eventstore"]
    dbCol = db["events-contracts"]
    query = {"payload.employeeTakeCare.id": id, "eventName" : "contractSentApprove" ,"commitStamp" : {"$gte" : fromDate, "$lte" : endDate} }
    numProDcontract = dbCol.find(query).count()
    return numProDcontract
    
#def getNumCustomerInput(id):
#    db = client["msx-contract-eventstore"]
#    dbCol = db["events-contracts"]
#    numCustomerInput = 0
#    agr = [
#        {
#            "$match": {
#                   "payload.employeeTakeCareId": id,
#                   "eventName": "contractApproved",
#                   "commitStamp" : {"$gte" : fromDate, "$lte" : endDate}
#                }
#        },
#        {"$group" : {"_id": "$id",
#           "payloads": { "$push": "$$ROOT" }}},
#        { "$count": "count_doc" }
#    ]
#    docs = dbCol.aggregate(agr, allowDiskUse=True)
#    for doc in docs:
#        numCustomerInput = doc["count_doc"]
#        break
#    return numCustomerInput
    
def getLoginInfoFromUser(user):
    db = client["msx-sts-eventstore"]
    dbCol = db["events-auths"]
    query = {"streamId": user["id"], "commitStamp" : {"$gte" : fromDate, "$lte" : endDate}}
    select = {"commitStamp":1, "eventName":1}
    docs = dbCol.find(query, select)
    output = userTemplate.copy()
    count = docs.count()
    arrayLogin = []
    output["id"] = user["id"]
    output["manv"]= user["code"]
    output["name"] = user["name"]
    output["ttgd"] = user["pos"]["name"]
    output["email"] = user["email"]
    if count > 0:
        for doc in docs:
            if doc["eventName"] ==  "userSignedIn":
                date = doc["commitStamp"].strftime("%d/%m/%Y - %H:%M:%S")
                arrayLogin.append(date)           
    if len(arrayLogin) > 0:
        output["firstLoginDate"] = arrayLogin[0]
        output["lastLoginDate"] = arrayLogin[len(arrayLogin) - 1]
        output["numLogin"]= len(arrayLogin)
        for arr in arrayLogin:
            output["listLogin"] = arr + ","            
    return output

def getNumCallFrom(id):
    arrayFrom = []
    doc = getListAccount()
    numCallFrom = 0
#    for i in range(2, row + 1):
#        str = sheet.cell(row = i, column = 3)
#        if str.value != None: 
#            a=str.value
#            a2=a.replace(".","@",1)
#            arrayFrom.append(a2)
#    for x in range(len(arrayFrom)):
#          if id == arrayFrom[x]:
#              numCallFrom = numCallFrom + 1
    return numCallFrom
    
def getNumCallTo(id):
     arrayTo = []
     numCallTo = 0
#     for i in range(2, row + 1):
#        str2 = sheet.cell(row = i, column = 5)
#        if str2.value != None:    
#            b=str2.value
#            b2=b.replace(".","@",1)
#            arrayTo.append(b2)
#     for x in range(len(arrayTo)):
#         if id == arrayTo[x]:
#              numCallTo = numCallTo + 1
     return numCallTo
def getDataUser():
    arrOutput = []
    listAcc = getListAccount() 
    for doc in listAcc:
        user = getLoginInfoFromUser(doc)
        if user["id"] != "":
            arrOutput.append(user)
    for doc in arrOutput:     
        doc["numDemand"] = getNumDemandCreated(doc["id"])
        doc["numDemandBroker"] = getNumDemandW(doc["id"])
        doc["numConsignment"] = getNumConsignmentCreated(doc["id"])
        doc["numConsignmentBroker"] = getNumConsignmentW(doc["id"])
        doc["numInput"] = doc["numDemandBroker"] + doc["numConsignmentBroker"]
        doc["numInputE"] = doc["numDemand"] + doc["numConsignment"]
        doc["numContractApprove"] = getNumContractApproved(doc["id"])
        doc["numCustomerInput"] =  doc["numCusConsign"] + doc["numCusDemand"]
        doc["numCallFrom"] = getNumCallFrom(doc["email"])
        doc["numCallTo"] = getNumCallTo(doc["email"])
        doc["numCusConsign"] = getNumCustomerConsign(doc["id"])
        doc["numCusDemand"] = getNumCusDemand(doc["id"])
        doc["numProDcontract"] = getNumProDcontract(doc["id"])
        doc["numProContract"] = getNumContractApproved(doc["id"])
        doc["numPro"] = doc["numProDcontract"] + doc["numProContract"]
    return arrOutput
    
######
# END REPORT USER
#####
#####



def exportExcel(data, sheet, templateColumnIndex):
    row = 0
    for i in range(0, len(data) - 1):
        for key in templateColumnIndex:
            col = templateColumnIndex[key]
            sheet.cell(row + rowBegin, col).value = data[i][key]
        sheet.cell(row + rowBegin, 1).value = row + 1
        row = row + 1
    myWorkbook.save(outputFile)
    
def main():
    exportExcel(getDataUser(), sheetUser, userColumnIndex)
main()
