const UnitCollection = db.getMongo().getDB('msx-matching-readmodel').getCollection('property-units');
const UnitAttributeCollection = db.getMongo().getDB('msx-matching-readmodel').getCollection('unit-attributes_copy_dungs');
const DemandAttributeCollection = db.getMongo().getDB('msx-matching-readmodel').getCollection('demand-attributes_copy_dungs');

function findUnitAttributeLocations(){
    const atts = UnitAttributeCollection.find({'key' : 'location'}).toArray();
	return atts;
}

//thêm addess vào Attribut và remove những row nào có key là location
function updateUnitAtrribute(att){
	printjson(att.unitId);
	printjson(att.text);
    const query = {'unitId' : att.unitId };
    const set = { '$set': {'address' : att.text}};
    UnitAttributeCollection.updateMany(query, set);
    UnitAttributeCollection.deleteOne({'unitId' : att.unitId, 'key':'location'});
}

function findDemandAttributeLocations(){
    const atts = DemandAttributeCollection.find({'key' : 'location'}).toArray();
	return atts;
}

//thêm addess vào Attribut và remove những row nào có key là location
function updateDemandAtrribute(att){
	printjson(att.demandId);
	printjson(att.value);
    const query = {'demandId' : att.demandId };
    const set = { '$set': {'address' : att.value}};
    DemandAttributeCollection.updateMany(query, set);
    DemandAttributeCollection.deleteOne({'demandId' : att.demandId, 'key':'location'});
}

function findUnitCollection(){
	const atts = UnitCollection.find({}).toArray();
	return atts;
}

//thêm addess vào Attribut và remove những row nào có key là location
function updateUnitAtrributeByUnit(unit){
	printjson(unit.unit.id);
	printjson(unit.unit.address);
    const query = {'unitId' : unit.unit.id, address:{$eq:null}};
    const set = { '$set': {'address' : unit.unit.address}};
    UnitAttributeCollection.updateMany(query, set);
}

function main(){
    // const unitAtts = findUnitAttributeLocations();
	// for(let att of unitAtts){
		// updateUnitAtrribute(att);
	// }
	
	// const demandAtts = findDemandAttributeLocations();
	// for(let att of demandAtts){
		// updateDemandAtrribute(att);
	// }
	
	const units = findUnitCollection();
	for(let unit of units){
		updateUnitAtrributeByUnit(unit);
	}
}
main();