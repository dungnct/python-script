
// const projectId = 'f105fd59-ab80-459e-853c-7c6a2c029008';
const Unit = db.getMongo().getDB(dbProperty).getCollection('property-units');//ycdch,ycdco
const Tranx = db.getMongo().getDB(dbProperty).getCollection('primary-transactions');//ycdch,ycdco
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	  const Units = Unit.find({
			'project.id': projectId,
			code: /.+\.\d+/
		}).toArray();
		log(Units);
	for (let i = 0; i < Units.length; i++) {
		const unit = Units[i];
		log(unit.id);
		const code = unit.code.replace('.', '-');
		Unit.updateOne({id: unit.id}, {$set: {
			code: code,
		}});
		Tranx.updateMany({'propertyUnit.id': unit.id}, {$set: {
			'propertyUnit.code': code,
		}});
	}
}
main();