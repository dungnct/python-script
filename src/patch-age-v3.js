const dbName = 'th-msx-property-readmodel';
const Tranx = db.getMongo().getDB(dbName).getCollection('primary-transactions');//ycdch,ycdco
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	//bithday type is date
	Tranx.update(
    {
		'customer.info.age': {$exists: false},
        $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : {$type: "date"}
    },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate", "$customer.info.birthday" 
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )//%Y-%m-%d
	Tranx.update(
    {
		'customer.info.age': {$exists: false},
        $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /\d{4}-\d{1,2}-\d{1,2}T*/
    },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate",
                {"$toDate": '$customer.info.birthday'}
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )
	//%d-%m-%Y
	Tranx.update(
    {
		'customer.info.age': {$exists: false},
        $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /\d{1,2}-\d{1,2}-\d{4}/
      },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate",
                {
                  "$dateFromString": {
					"dateString": "$customer.info.birthday",
					"format": "%Y-%m-%d",
				  },
                }
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )
	//%d/%m/%Y
	Tranx.update(
    {
		'customer.info.age': {$exists: false},
          $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /\d{1,2}\/\d{1,2}\/\d{4}/
      },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate",
                {
                  "$dateFromString": {
					"dateString": "$customer.info.birthday",
					"format": "%d/%m/%Y",
				  },
                }
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )
	//%Y/%m/%d
	Tranx.update(
    {
		'customer.info.age': {$exists: false},
          $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /\d{4}\/\d{1,2}\/\d{1,2}/
      },
        [{$set: {"customer.info.age": {
          "$divide": [
            {
              "$subtract": [
                "$createdDate",
                {
                  "$dateFromString": {
					"dateString": "$customer.info.birthday",
					"format": "%Y/%m/%d",
				  },
                }
              ]
            },
            (365.25 * 24*60*60*1000)
          ]
        }}}], 
        {multi: true}
    )
	//onlyYear
    Tranx.update(
      {
		  'customer.info.age': {$exists: false},
        "customer.info.onlyYear": { $eq: true },
        "customer.info.birthdayYear" : /^\d{4}$/
      },
          [{$set: {"customer.info.age": {
            "$divide": [
              {
                "$subtract": [
                  "$createdDate",
                  {
                    "$toDate": {
                      "$concat": [
                        "$customer.info.birthdayYear",
                        "-01-01"
                      ]
                    },
                  }
                ]
              },
              (365.25 * 24*60*60*1000)
            ]
          }}}], 
          {multi: true}
      )
	//Coordinated Universal Time
	Tranxs = Tranx.find(
    {
		'customer.info.age': {$exists: false},
          $or: [{"customer.info.onlyYear": { $exists: false }}, {"customer.info.onlyYear": { $eq: false }}],
          "customer.info.birthday" : /Coordinated Universal Time/
      }).toArray();
	// log(Tranxs.length);
    for (let i = 0; i < Tranxs.length; i++) {
		const tranx = Tranxs[i];
		log(tranx.id);
		let birthday = new Date(tranx.customer.info.birthday);
		log(birthday);
		const age = (tranx.createdDate - birthday)/(365.25 * 24*60*60*1000);
		log(age);
		Tranx.updateOne({id: tranx.id}, {$set: {"customer.info.age": age}});
    }
}
main();