// const projectId = '12e211aa-f971-41d0-a2e9-99a84f6adf85';//Charm City Bình Dương dev
const projectId = '0d4235d6-4b59-44fd-8b31-63bf972443d5';//Project test 0507 dev
// const projectId = '5eefa652-6123-48d1-a301-1894d70a8499';//Charm City Demo 2 052020 staging
const PropertyTicket = db.getMongo().getDB('msx-property-readmodel').getCollection('property-tickets');//ycdch,ycdco
const PropertyPrimaryTransaction = db.getMongo().getDB('msx-property-readmodel').getCollection('property-primary-transactions');//dsgd
const Transaction = db.getMongo().getDB('msx-transaction-readmodel').getCollection('transactions');//phieuthu
const TransactionPatching = db.getMongo().getDB('msx-transaction-readmodel').getCollection('transactions-patching');//phieuthu patching
const PrimaryTransactionsPatching = db.getMongo().getDB('msx-property-readmodel').getCollection('primary-transactions-patching');
const Project = db.getMongo().getDB('msx-property-readmodel').getCollection('projects');
function log(obj){
	print(JSON.stringify(obj));
}
function getCustomerMapping(customer, employee=null) {
	return {
		code: customer.code,
		info: {
			gender: customer.gender,
			birthday: customer.birthday,
			address: customer.address,
			rootAddress: customer.rootAddress,
			taxCode: customer.taxCode,
		},
		bankInfo: customer.bankInfo,
		personalInfo: {
			email: customer.email,
			phone: customer.phone,
			name: customer.name,
			identities: [{
				value: customer.identityNumber,
				date: customer.identityIssueDate,
				place: customer.identityIssueLocation,
			}]
		},
		employee: customer.employeeTakeCare ? customer.employeeTakeCare: employee || null,
		identities: [{
			value: customer.identityNumber,
			date: customer.identityIssueDate,
			place: customer.identityIssueLocation,
		}],
		identityNumber: customer.identityNumber,
		identityDate: customer.identityIssueDate,
		identityPlace: customer.identityIssueLocation,
		email: customer.email,
		phone: customer.phone,
		name: customer.name,
	}
}
function findProject(){
	return Project.findOne({'id': projectId}, {type: 1, name: 1, setting:1});
}
function findTicket(){
	return PropertyTicket.find({'projectId': projectId}).toArray();
}
function findDsgdSuccess(){
	return PrimaryTransaction.find({'project.id': projectId}).toArray();
}
function findDsgdUnsuccess(){
	return PrimaryTransaction.find({'project.id': projectId}).toArray();
}
function findTransaction(){
	return PrimaryTransaction.find({'propertyTicket.projectId': projectId}).toArray();
}

//mỗi ticket tạo 1 Tranx
//nếu YCDC có refTicketCode thì chỉ update Tranx ngược lại tạo 1 Tranx
function patchTicket(project){
	//code -> bookingTicketCode
	//employeeTakeCare -> employee
	//projectId-->project
	// const customer = getCustomerMapping(ele.customer, null);
	let list = PropertyTicket.find({'projectId': projectId}).toArray();
	let listInsert = [];
	for (let i = 0; i < list.length; i++) {
		const ele = list[i];
		ele.employee = ele.employeeTakeCare;
		ele.customer = getCustomerMapping(ele.customer, ele.employee);
		ele.project = project;
		delete ele.employeeTakeCare;
		delete ele.projectId;
		delete ele.settingProject;
		if(ele.ticketType === 'YCDCH'){
			ele.bookingTicketCode = ele.code;
			delete ele.code;
			log(ele._id);
			listInsert.push(ele);
		} else if(ele.ticketType === 'YCDC'){
			if(ele.refTicketCode){
				ele.escrowTicketCode = ele.code;
				delete ele.code;
				const oldTicket = PropertyTicket.findOne({'id': ele.refTicketCode});
				ele.bookingTicketCode = oldTicket.code;
				delete ele.refTicketCode
				// ele.refTicketCode = oldTicket.bookingTicketCode;  //Huy: Tại sao vẫn còn refTicketCode trong khi đã có bookingCode với escrowCode? có nên giữ cả code với refCode để phân biết dữ liệu tạo mới và dữ liệu migrate?
				PrimaryTransactionsPatching.update({'bookingTicketCode': ele.bookingTicketCode}, {$set: ele});
			} else {
				ele.escrowTicketCode = ele.code;
				delete ele.code;
				listInsert.push(ele);
			}
		}
		
	}
	log(list[0]);
	PrimaryTransactionsPatching.insert(listInsert, {ordered: true});
}

//patching phiếu thu
//update lại propertyTicket giống với Tranx
function patchTransaction(project){
	//code -> bookingTicketCode
	//employeeTakeCare -> employee
	//projectId-->project
	// const customer = getCustomerMapping(ele.customer, null);
	let list = Transaction.find({'propertyTicket.projectId': projectId}).toArray();
	// let listInsert = [];
	for (let i = 0; i < list.length; i++) {
		const model = list[i];
		const ele = model.propertyTicket;
		ele.employee = ele.employeeTakeCare;
		ele.customer = getCustomerMapping(ele.customer, ele.employee);
		ele.project = project;
		delete ele.employeeTakeCare;
		delete ele.projectId;
		delete ele.settingProject;
		if(ele.ticketType === 'YCDCH'){
			ele.bookingTicketCode = ele.code;
			delete ele.code;
			log(ele._id);
			model.propertyTicket = ele;
			// listInsert.push(model);
			TransactionPatching.update({'id': model.id}, {$set: model}, { upsert: true });
		} else if(ele.ticketType === 'YCDC'){
			if(ele.refTicketCode){
				ele.escrowTicketCode = ele.code;
				delete ele.code;
				const oldTicket = PropertyTicket.findOne({'id': ele.refTicketCode});
				ele.bookingTicketCode = oldTicket.code;
				delete ele.refTicketCode
				// ele.refTicketCode = oldTicket.bookingTicketCode; //Huy: Tương tự, chỗ này có cần refTicketCode không?
				model.propertyTicket = ele;
				TransactionPatching.update({'id': model.id}, {$set: model}, { upsert: true });
			} else {
				ele.escrowTicketCode = ele.code;
				delete ele.code;
				model.propertyTicket = ele;
				// listInsert.push({}, {$set: model});
				TransactionPatching.update({'id': model.id}, {$set: model}, { upsert: true });
			}
		}	
	}
}

//patching property-primary-transactions viết tắt pTran
//patching property-primary-transactions không thàng công
function patchTranxSuccess(){
	//tìm trong property-primary-transactions có (this.ticket.id === ticket.id và this.status === SUCCESS và this.customerServiceStatus=APPROVED) 
	let pTrans = PropertyPrimaryTransaction.find({'project.id': projectId, 'status':'SUCCESS', 'customerServiceStatus':'APPROVED'}).toArray();
	for (let i = 0; i < pTrans.length; i++) {
		const pTran = pTrans[i];
		//nếu có gắn với 1 ticket
		if(pTran.ticket) {
			const oldTranx = PrimaryTransactionsPatching.findOne({'id': pTran.ticket.id});
			oldTranx.status = 'SUCCESS';
			oldTranx.propertyUnit = pTran.propertyUnit;
			oldTranx.employee = pTran.employee ? pTran.employee : oldTranx.employee; //Huy: Đoạn này override data, có cần check theo thứ tự thời gian không?
			oldTranx.customer = pTran.customer ? pTran.customer : oldTranx.customer; //Huy: Đoạn này override data, có cần check theo thứ tự thời gian không?
			oldTranx.pos = pTran.pos ? pTran.pos : oldTranx.pos;
			PrimaryTransactionsPatching.update({'id': pTran.ticket.id}, {$set: oldTranx});
		} else {
			PrimaryTransactionsPatching.insert(pTran);
		}
	}
}
function patchTranxUnSuccess(){
	let pTrans = PropertyPrimaryTransaction.find({
		'project.id': projectId, 
		$or: [
			{'status':{$ne: 'SUCCESS'}},
			{'customerServiceStatus':{$ne: 'APPROVED'}}
		],
		}).toArray();
	for (let i = 0; i < pTrans.length; i++) {
		const pTran = pTrans[i];
		if(pTran.ticket) {
			const oldTranx = PrimaryTransactionsPatching.findOne({'id': pTran.ticket.id});
			if(pTran.status === 'SUCCESS'){
				oldTranx.status = 'POS_CONFIRM';
			} else {
				oldTranx.status = pTran.status;
			}
			oldTranx.propertyUnit = pTran.propertyUnit;
			oldTranx.employee = pTran.employee ? pTran.employee : oldTranx.employee; //Huy: Đoạn này override data, có cần check theo thứ tự thời gian không?
			oldTranx.customer = pTran.customer ? pTran.customer : oldTranx.customer; //Huy: Đoạn này override data, có cần check theo thứ tự thời gian không?
			oldTranx.pos = pTran.pos ? pTran.pos : oldTranx.pos;
			PrimaryTransactionsPatching.update({'id': pTran.ticket.id}, {$set: oldTranx});
		} 
		// else {
			// if(pTran.status === 'SUCCESS'){
				// pTran.status = 'POS_CONFIRM';
			// }
			// PrimaryTransactionsPatching.insert(pTran);
		// }
	}
}

function main(){
	const project = findProject();
	patchTicket(project);
	patchTransaction(project);
	patchTranxSuccess();
	patchTranxUnSuccess();

	//Huy: Đang đẩy dữ liệu vào các collection patching rồi làm gì tiếp theo?
}
main();