// const projectId = 'ddce1381-ad8b-4019-b6e4-ecdca9db0215';//Gem City 13/06/2020 thuc hanh
// const projectId = '19137f02-3888-4746-a7f8-89b0d90c7ea5';//DA3 - GEM
// const dbType = th or prod or empty
if(dbType){
	dbType = dbType + '-';
	log(dbType);
}
const Tranx = db.getMongo().getDB(dbType + 'msx-property-readmodel').getCollection('primary-transactions');//ycdch,ycdco
const EventProject = db.getMongo().getDB(dbType + 'msx-property-readmodel').getCollection('event-projects');//ycdch,ycdco
const Unit = db.getMongo().getDB(dbType + 'msx-property-readmodel').getCollection('property-units');//ycdch,ycdco
const Project = db.getMongo().getDB(dbType + 'msx-property-readmodel').getCollection('projects');//ycdch,ycdco
const SaleProgram = db.getMongo().getDB(dbType + 'msx-property-readmodel').getCollection('sales-programs');//ycdch,ycdco
const Transaction = db.getMongo().getDB(dbType + 'msx-transaction-readmodel').getCollection('transactions');//phieuthu
function log(obj){
	print(JSON.stringify(obj));
}

function deleteProject(projectId) {
	Project.deleteMany({id: projectId});
}
function deleteEventProject(projectId) {
	EventProject.deleteMany({projectId: projectId});
}
function deleteTranx(projectId) {
	Tranx.deleteMany({'project.id': projectId});
}
function deleteTransaction(projectId) {
	Transaction.deleteMany({'propertyTicket.project.id': projectId});
}
function deleteSaleProgram(projectId) {
	SaleProgram.deleteMany({'project.id': projectId});
}
function deleteUnit(projectId) {
	Unit.deleteMany({'project.id': projectId});
}

function main(){
	deleteProject(projectId);
	deleteEventProject(projectId);
	deleteTranx(projectId);
	deleteUnit(projectId);
	deleteSaleProgram(projectId);
	deleteTransaction(projectId);
	// insertProject(projectId);
	// insertEventProject(projectId);
	// insertTranx(projectId);
	// insertUnit(projectId);
	// insertTransaction(projectId);
}
main();