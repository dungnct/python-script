
// const projectId = 'f105fd59-ab80-459e-853c-7c6a2c029008';
const Unit = db.getMongo().getDB(dbProperty).getCollection('property-units');//ycdch,ycdco
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	  const Units = Unit.find({
			'project.id': projectId,
		}).toArray();
		log(projectId);
	for (let i = 0; i < Units.length; i++) {
		const unit = Units[i];
		const view1 = unit.attributes.find(u => u.id === '945c3fae-bf91-4fef-95a1-9e58a3a3233c');
		const block = unit.attributes.find(u => u.id === '0f76e8fc-ad24-47d3-ad4b-987f8fd22fea');
		const floor = unit.attributes.find(u => u.id === '00313c2b-ee59-4760-ad51-bf504fd1a5e9');
		
		Unit.updateOne({id: unit.id}, {$set: {
			view1: view1.value || '',
			block: block.value || '',
			floor: floor.value || '',
		}});
	}
}
main();