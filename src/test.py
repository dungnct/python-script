import sys
import json
import pymongo
import shutil
from datetime import datetime, date
from unidecode import unidecode
import unicodedata

hostsource = sys.argv[1]
portsource = sys.argv[2]


clientsource = pymongo.MongoClient(hostsource, int(portsource))
# Code=clientsource["msx-customer-readmodel"]["codes-test"]
# newObj = Code.find_one_and_update({ 'name': 'contract.index' },{ '$set': { 'prefix': 'KH-', 'suffix': '' }, '$inc': { 'index': int(1) } },{ 'upsert': True})
# newCode = Code.find_one(newObj)
# print(newCode)

Customer=clientsource["msx-customer-readmodel"]["customers-test"]
docs = Customer.find({"$or":[ {"code":''}, {"code":{'$exists':False}}, {"code":{'$eq':None}}]})
for customer in docs:
    print(customer)
