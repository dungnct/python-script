// const projectId = '2860642f-3233-438d-b1d6-b013a169bb0a';
// const saleProgramId = 'efbc7a56-9fb2-48b8-ac22-256ee43b742f';
// const unitDb = 'th-msx-property-readmodel';
// const orgchartDb = 'th-msx-orgchart-readmodel';
const Unit = db.getMongo().getDB(unitDb).getCollection('property-units');
const OrgChart = db.getMongo().getDB(orgchartDb).getCollection('orgcharts');
function log(obj){
	print(JSON.stringify(obj));
}

function main(){
	const units = Unit.find({
		'project.id': projectId,
		'salesProgram.id': saleProgramId,
		primaryStatus: {$in: ['SUCCESS', 'LOCK_CONFIRM']},
		"$expr": { "$isArray": '$priorities' },
	}).toArray();
	for(let i = 0; i < units.length; i++) {
		const unit = units[i];
		log(unit.id);
		const priority = unit.priorities.find(pr => ['SUCCESS', 'LOCK_CONFIRM'].includes(pr.status));
		if(priority && unit.pos && priority.posId !== unit.pos.id){
			log(priority.priority);
			const san = OrgChart.findOne({id: priority.posId});
			Unit.updateOne({id: unit.id}, {
				$set: {
					pos : {id: san.id, name: san.name, parentId: san.parentId},
					priorities: [priority]
				}
			})
		}
	}
}
main();